<?php namespace Vitae\Application\Models;

use Model;
use BackendAuth;

/**
 * Preference Model
 */
class Preference extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Behaviors implemented by this model.
     */
    public $implement = ['Backend.Behaviors.UserPreferencesModel'];

    /**
     * @var string The database table used by the model.
     */
    protected $table = 'backend_user_preferences';

    /**
     * @var string Unique code
     */
    public $settingsCode = 'backend::application.preferences';

    /**
     * @var mixed Settings form field defitions
     */
    public $settingsFields = 'fields.yaml';

    /**
     * @var array Validation rules
     */
    public $rules = [];

}
<?php namespace Vitae\Application\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateApplicationsTable extends Migration
{
    public function up()
    {
        Schema::create('vitae_application_applications', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title', 255);
            $table->string('company', 255);
            $table->string('contact', 255);
            $table->string('email', 255);
            $table->text('coveringtext')->nullable();
            $table->text('preferences')->nullable();
            $table->string('applicationurl', 32);
            $table->string('authtoken', 6);
            $table->boolean('active')->default(1);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('vitae_application_applications');
    }
}

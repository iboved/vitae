<?php

return [
    'plugin' => [
        'name' => 'Octobase Categories',
        'description' => 'Configurable Category Plugin',
    ],
    'categories' => [
        'model' => [
            'name' => 'Category',
            'title' => 'Categories',
            'create' => 'Create Category',
            'update' => 'Update Category',
            'reorder' => 'Reorder Categories',
            'open_nodes' => 'Open all nodes',
            'close_nodes' => 'Close all nodes',
            'empty_option' => '-- not selected --',
        ],
        'labels' => [
            'title' => 'Title',
            'parent' => 'Parent category',
            'is_active' => 'Active',
        ],
        'errors' => [
            'max_depth' => 'This depth is not allowed',
        ],
    ],
    'settings' => [
        'tabs' => [
            'categories' => 'Categories',
        ],
        'labels' => [
            'show_navigation' => 'Show navigation',
            'show_active' => 'Show active',
            'tree_expanded' => 'Tree expanded',
            'max_depth' => 'Max depth',
        ],
    ],
];

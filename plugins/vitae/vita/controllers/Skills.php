<?php namespace Vitae\Vita\Controllers;

use Backend\Classes\Controller;
use Backend\Models\User;
use BackendMenu;
use BackendAuth;
use Session;
use Vitae\Vita\Models\Skill;

class Skills extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController',
        'Vitae\Account\Behaviors\ModalController',
        'Vitae\Account\Behaviors\MultiTenantController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();

        $this->pageTitle = 'vitae.vita::lang.skills.model.title';

        BackendMenu::setContext('Vitae.Vita', 'vita', 'skills');
    }
    
    public function index()
    {
        if (BackendAuth::getUser()->is_superuser)
        {
            $this->vars['items_count'] = Skill::where('user_id', Session::get('user_id'))->count();
        } 
        else 
        {
            $this->vars['items_count'] = Skill::where('user_id', BackendAuth::getUser()->id)->count();
        }

        return $this->asExtension('ListController')->index();
    }

    public function onFiltered()
    {
        $data = $this->listRefresh();

        $data['items_count'] = Skill::where('user_id', Session::get('user_id'))->count();

        return $data;
    }

    public function index_onDelete()
    {
        $data = parent::index_onDelete();

        if (BackendAuth::getUser()->is_superuser)
        {
            $data['items_count'] = Skill::where('user_id', Session::get('user_id'))->count();
        }
        else
        {
            $data['items_count'] = Skill::where('user_id', BackendAuth::getUser()->id)->count();
        }

        return $data;
    }
}
<?php namespace Stargate\Gatepedia\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Series extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'stargate.gatepedia.manage_gatepedia' 
    ];

    public function __construct()
    {
        parent::__construct();

        $this->bodyClass = 'compact-container';
        
        BackendMenu::setContext('Stargate.Gatepedia', 'gatepedia', 'series');
    }

}
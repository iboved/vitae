<?php

namespace Vitae\Account\Traits;

use BackendAuth;
use Illuminate\Support\Facades\Session;

trait ModelEventTrait
{
    public function afterCreate()
    {
        $user = BackendAuth::getUser();

        Session::put('status_widget.' . self::class, self::where('user_id', $user->id)->count());
    }

    public function afterDelete()
    {
        $user = BackendAuth::getUser();

        Session::put('status_widget.' . self::class, self::where('user_id', $user->id)->count());
    }
}

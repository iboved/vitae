<?php namespace Stargate\Project\Console;

use DB;
use Hash;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Backend\Models\User;

class DBSync extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'project:db-sync';

    /**
     * @var string The console command description.
     */
    protected $description = 'Syncs the DB';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $defaultDBConnection = config('database.default');

        $localUser = config("database.connections.{$defaultDBConnection}.username");
        $localPassword = config("database.connections.{$defaultDBConnection}.password");
        $localDb = config("database.connections.{$defaultDBConnection}.database");

        $remoteHost = config("database.connections.db-sync.host");
        $remoteHostUsername = config("database.connections.db-sync.host_username");
        $remoteUser = config("database.connections.db-sync.username");
        $remotepassword = config("database.connections.db-sync.password");
        $remoteDb = config("database.connections.db-sync.database");

        $command = <<<EOT
mysqldump -u $localUser -p$localPassword $localDb | gzip | ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $remoteHostUsername@$remoteHost \
"gunzip | mysql -u $remoteUser -p$remotepassword $remoteDb"
EOT;

        $this->output->writeln(exec($command));
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}

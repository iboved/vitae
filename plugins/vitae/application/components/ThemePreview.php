<?php namespace Vitae\Application\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Theme;

class ThemePreview extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'ThemePreview Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $theme = Theme::load(input('theme'));

        if ($theme->hasCustomData() && array_key_exists(input('color'), $theme->getConfigArray('form.fields.color.options'))) {
            $this->page['theme_color'] = input('color');
        }
    }
}

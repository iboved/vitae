<?php namespace Kay899\Band\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSongablesTable extends Migration
{
    public function up()
    {
        Schema::create('songables', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('song_id');
            $table->integer('songable_id');
            $table->string('songable_type');
        });
    }

    public function down()
    {
        Schema::dropIfExists('songables');
    }
}

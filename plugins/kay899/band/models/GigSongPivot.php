<?php namespace  Kay899\Band\Models;

use October\Rain\Database\Pivot;
use October\Rain\Database\Traits\Sortable;
use October\Rain\Database\Traits\Validation;

/**
 * Gig-Song Pivot Model
 */
class GigSongPivot extends Pivot
{
    use Validation, Sortable;

    const SORT_ORDER = 'gig_sort_order';

    /**
     * @var array Rules
     */
    public $rules = [
        'set' => 'required|integer',
    ];

	public $attributeNames = [
        'set' => 'kay899.band::lang.gigs.labels.sets',
    ];

	public function beforeSave()
	{
		if (empty($this->gig_sort_order)) {
			$this->gig_sort_order = $this->id;
		}
	}

    public function getSetLabelAttribute()
    {
    	if ($this->set != 99)
    	{
    		return trans('kay899.band::lang.gigs.labels.set') . ' ' . $this->set;
    	}
    	else 
    	{
    		return trans('kay899.band::lang.gigs.labels.add_ons');
    	}
    }
}
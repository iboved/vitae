<?php namespace Octobase\Mediamanager\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Octobase\Backend\Models\Setting;

/**
 * Videos Back-end Controller
 */
class Videos extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend\Behaviors\ReorderController',
        'Octobase\Backend\Behaviors\ModalController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('October.Backend', 'media', 'videos');
    }

    public function listGetConfig($definition = null)
    {
        $config = $this->asExtension('ListController')->listGetConfig($definition);

        if (Setting::get('mediamanager_show_is_active_in_youtube_module')) {
            $config->list = '$/octobase/mediamanager/models/video/columns_active.yaml';
        }

        return $config;
    }
}

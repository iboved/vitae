<?php namespace Vitae\Application\Models;

use Model;
use Session;
use BackendAuth;
use Yaml;
use Vitae\Application\Models\Preference;

/**
 * Application Model
 */
class Application extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
        'title' => 'required',
        'company' => 'required',
        'contact' => 'required',
        'email' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'vitae_application_applications';

    protected $jsonable = ['preferences'];

    /**
     * User relation to show user details in lists
     */
    public $belongsTo = [
        'user' => ['Backend\Models\User'],
    ];

    /**
     * Action relation to show action details in lists
     */
    public $hasMany = [
        'actions' => 'Vitae\Application\Models\Action'
    ];

    /**
     * Sets user_id to current logged in user or selected user by Admin
     */
    public function beforeCreate()
    {
        if (BackendAuth::getUser()->is_superuser && Session::has('user_id'))
        {
            $this->user_id = Session::get('user_id');
        } 
        else
        {
            $this->user_id = BackendAuth::getUser()->id;
        }

        $this->applicationurl = str_random(32);
        $this->authtoken = mt_rand(100000, 999999);
    }

    /**
     * Sets fields with the Users Preference Default values
     */
    public function filterFields($fields, $context = null)
    {
        if ($context == 'create')
        {
            $preference = Preference::instance();

            $yaml_fields = Yaml::parseFile(plugins_path().'/vitae/application/models/preference/fields.yaml');

            foreach ($yaml_fields['tabs']['fields'] as $yaml_key => $yaml_field)
            {
                 if ($yaml_field['type'] != 'section' && $yaml_field['type'] != 'themeselector')
                {
                    if (!empty($preference->attributes))
                    {
                        $fields->{'preferences['.$yaml_key.']'}->value = $preference->$yaml_key;
                    }
                    else
                    {
                        $fields->{'preferences['.$yaml_key.']'}->value = $fields->{'preferences['.$yaml_key.']'}->defaults;
                    }
                }
            }
            // foreach() replaces this:
            // $fields->{'preferences[show_skills]'}->value = $preference->show_skills;
        }
    }
}

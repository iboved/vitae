<?php namespace Stargate\Project\Models;

use Model;

/**
 * Model
 */
class Slug extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'stargate_project_sluggable';

    protected $guarded = ['id'];

    /**
     * Relations
     */
    public $morphTo = [
        'sluggable' => []
    ];
}
<?php namespace Kay899\Band\Traits;

use BackendAuth;

trait Base
{
    public function beforeCreate()
    {
        if (php_sapi_name() != 'cli') {
            $this->created_by = BackendAuth::getUser()->id;
            $this->updated_by = BackendAuth::getUser()->id;
        }
    }

    public function beforeUpdate($id = null)
    {
        $this->updated_by = BackendAuth::getUser()->id;
    }

    public function beforeDelete($id = null)
    {
        $this->updated_by = BackendAuth::getUser()->id;
        $this->deleted_by = BackendAuth::getUser()->id;
        $this->save();
    }
}
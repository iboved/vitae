<?php namespace Vitae\Vita\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProfilesTable extends Migration
{
    public function up()
    {
        Schema::create('vitae_vita_profiles', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->tinyInteger('salutation')->nullable();
            $table->string('title', 15)->nullable();
            $table->string('nationality')->nullable();
            $table->string('drivinglicence')->nullable();
            $table->timestamp('birthday')->nullable();
            $table->string('birthplace')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('street')->nullable();
            $table->string('zip', 15)->nullable();
            $table->string('city')->nullable();
            $table->integer('country_id')->unsigned()->nullable()->index();
            $table->string('familystatus', 2)->nullable();
            $table->string('denomination', 2)->nullable();
            $table->string('children')->nullable();
            $table->string('parents')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('vitae_vita_profiles');
    }
}

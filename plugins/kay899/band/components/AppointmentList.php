<?php namespace Kay899\Band\Components;

use Cms\Classes\ComponentBase;
use Kay899\Band\Models\Appointment;
use Twig_Environment;
use Carbon\Carbon;

class AppointmentList extends ComponentBase
{
    /**
     * Array of appointmentlist
     * @var array
     */
    public $appointmentlist;

    public function componentDetails()
    {
        return [
            'name'        => 'AppointmentList',
            'description' => 'List with appointments, no absences'
        ];
    }

    // This array becomes available on the page as {{ component.appointments }}
    public function appointments()
    {
        $yesterday = Carbon::now()->yesterday();

        return Appointment::where('type', '!=', 'absence')
            ->where('is_public', 1)
            ->where('date_from', '>', $yesterday)
            ->orderBy('date_from', 'asc')
            ->get();
    }
}

<?php namespace Vitae\Vita;

use Yaml;
use Event;
use System\Classes\PluginBase;
use Backend\Models\User as BackendUserModel;
use Backend\Models\Preference as BackendPreferenceModel;
use Backend\Controllers\Users as BackendUsersController;
use Vitae\Vita\Models\Profile as ProfileModel;
use Vitae\Vita\Controllers\Profile as ProfileController; 
use Vitae\Vita\Classes\Helper;

class Plugin extends PluginBase
{
    public function boot()
    {
    	BackendUserModel::extend(function($model)
    	{
            // Add relation for Profile
    		$model->hasOne['profile'] = ['Vitae\Vita\Models\Profile'];

            // Add relation for Subscription
    		$model->hasMany['subscriptions'] = ['Octobase\Cashier\Models\Subscription'];

            // Add values for Countries for Profile Form
            $model->implement[] = 'RainLab.Location.Behaviors.LocationModel';
    	});

    	Event::listen('backend.form.extendFields', function ($widget)
    	{
            if (!$widget->getController() instanceof BackendUsersController && !$widget->getController() instanceof ProfileController)
            {
                return;
            }

    		if (!$widget->model instanceof BackendUserModel)
            {
    			return;
            }

    		if (!$widget->model->exists)
            {
    			return;
            }

    		// Ensures that Profile always exists
    		ProfileModel::getFromUser($widget->model);

            // Extend the Profile Form Fields
            $widget->addTabFields(Yaml::parseFile(__DIR__ . '/models/profile/fields.yaml'));

            // Remove the Group Tab
            $widget->removeTab('backend::lang.user.groups');

            // Order Profile Tabs
            $fields = $widget->getTabs()->primary->fields;
            $orderTabs = ['vitae.vita::lang.profile.model.title'];
            $widget->getTabs()->primary->fields = array_merge(array_flip($orderTabs), $fields);

            // Ensures that login fields is not changeable
            $widget->getField('login')->readOnly = true;

    	});
    }
}

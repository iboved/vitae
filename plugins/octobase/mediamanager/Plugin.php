<?php namespace Octobase\MediaManager;

use App;
use Backend;
use BackendAuth;
use Event;
use Log;
use Input;
use Illuminate\Support\Facades\Validator;
use October\Rain\Exception\ValidationException;
use System\Classes\PluginBase;
use Backend\Widgets\MediaManager;
use System\Classes\MediaLibrary;
use Octobase\Backend\Models\Setting;
use Octobase\MediaManager\Models\Image;
use Octobase\MediaManager\Models\Folder;

/**
 * MediaManager Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'octobase.mediamanager::lang.plugin.name',
            'description' => 'octobase.mediamanger::lang.plugin.description',
            'author'      => 'Octobase',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Event::listen('backend.form.extendFields', function ($widget) {

            if (!$widget->model instanceof Setting) {
                return;
            }

            $widget->addTabFields([
                'mediamanager' => [
                    'label' => 'octobase.backend::lang.settings.model.title',
                    'type' => 'section',
                    'tab' => 'octobase.mediamanager::lang.settings.tabs.mediamanager',
                ],
                'mediamanager_is_active' => [
                    'label' => 'octobase.mediamanager::lang.settings.labels.is_active',
                    'type' => 'switch',
                    'span' => 'left',
                    'tab' => 'octobase.mediamanager::lang.settings.tabs.mediamanager',
                ],
                'mediamanager_show_is_gallery' => [
                    'label' => 'octobase.mediamanager::lang.settings.labels.show_is_gallery',
                    'type' => 'switch',
                    'span' => 'left',
                    'tab' => 'octobase.mediamanager::lang.settings.tabs.mediamanager',
                ],
                'mediamanager_show_copyright' => [
                    'label' => 'octobase.mediamanager::lang.settings.labels.show_copyright',
                    'type' => 'switch',
                    'span' => 'left',
                    'tab' => 'octobase.mediamanager::lang.settings.tabs.mediamanager',
                ],
                'mediamanager_show_youtube_module' => [
                    'label' => 'octobase.mediamanager::lang.settings.labels.show_youtube_module',
                    'type' => 'switch',
                    'span' => 'left',
                    'tab' => 'octobase.mediamanager::lang.settings.tabs.mediamanager',
                ],
                'mediamanager_show_is_active_in_youtube_module' => [
                    'label' => 'octobase.mediamanager::lang.settings.labels.show_youtube_is_active',
                    'type' => 'switch',
                    'span' => 'left',
                    'tab' => 'octobase.mediamanager::lang.settings.tabs.mediamanager',
                ],
            ]);
        });

        Event::listen('backend.menu.extendItems', function($manager) {

            $manager->addMainMenuItems('October.Backend', [
                'media' => [
                    'label'       => 'octobase.mediamanager::lang.plugin.name',
                    'icon'        => 'icon-object-group',
                    'iconSvg'     => '',
                    'code'        => 'media',
                    'url'         => Backend::url('backend/media')
                ],
            ]);

            $manager->addSideMenuItems('October.Backend', 'media', [
                'media' => [
                    'label'       => 'octobase.mediamanager::lang.plugin.name',
                    'icon'        => 'icon-object-group',
                    'iconSvg'     => '',
                    'code'        => 'media',
                    'url'         => Backend::url('backend/media')
                ],
                'folders' => [
                    'label'       => 'octobase.mediamanager::lang.folders.model.title',
                    'icon'        => 'icon-folder-open',
                    'iconSvg'     => '',
                    'code'        => 'folders',
                    'url'         => Backend::url('octobase/mediamanager/folders')
                ],
                'images' => [
                    'label'       => 'octobase.mediamanager::lang.images.model.title',
                    'icon'        => 'icon-image',
                    'iconSvg'     => '',
                    'code'        => 'images',
                    'url'         => Backend::url('octobase/mediamanager/images')
                ],
            ]);

            if (Setting::get('mediamanager_show_youtube_module')) {
                
                $manager->addSideMenuItems('October.Backend', 'media', [
                    'videos' => [
                        'label'       => 'octobase.mediamanager::lang.videos.model.title',
                        'icon'        => 'icon-youtube',
                        'iconSvg'     => '',
                        'code'        => 'videos',
                        'url'         => Backend::url('octobase/mediamanager/videos')
                    ],
                ]);
            }
        });

        if (Setting::get('mediamanager_is_active')) {
            
            MediaManager::extend(function ($widget) {
                $widget->addJs('/plugins/octobase/mediamanager/widgets/mediamanager/assets/js/mediamanager.js');
                $widget->addJs('/plugins/octobase/mediamanager/assets/js/input.preset.js');
                $widget->addViewPath(plugins_path().'/octobase/mediamanager/widgets/mediamanager/partials/');

                $widget->vars['without_title_count'] = Image::whereNull('title')->count();

                // Custom function, to store correct file extensions in DB
                $widget->addDynamicMethod('onLoadRenamePopupCustom', function() use ($widget) {
                    $path = Input::get('path');
                    $path = MediaLibrary::validatePath($path);

                    $type = Input::get('type');

                    $widget->vars['originalPath'] = $path;
                    $widget->vars['name'] = basename($path);
                    $widget->vars['listId'] = Input::get('listId');
                    $widget->vars['type'] = $type;

                    if ($type == 'folder') {
                        $folder = Folder::where('folder_name', basename($path))->first();

                        $widget->vars['title'] = $folder ? $folder->title : '';
                        $widget->vars['is_gallery'] = $folder ? $folder->is_gallery : '';
                    } else {
                        $extension = pathinfo($path, PATHINFO_EXTENSION);

                        $image = Image::where('file_url', 'storage/app/media' . $path)->first();

                        $widget->vars['extension'] = !empty($extension) ? '.' . $extension : '';
                        $widget->vars['title'] = $image ? $image->title : '';
                        $widget->vars['copyright'] = $image ? $image->copyright : '';

                        $config = $widget->makeConfig('$/octobase/mediamanager/models/image/media_fields.yaml');

                        if ($image) {
                            $config->model = $image;
                        } else {
                            $config->model = new Image();
                        }

                        $formWidget = $widget->makeWidget('Backend\Widgets\Form', $config);

                        $widget->vars['formWidget'] = $formWidget;
                    }

                    return $widget->makePartial('rename-form');
                });

                $widget->addDynamicMethod('onLoadRCreatePopupCustom', function() use ($widget) {
                    return $widget->makePartial('new-folder-form');
                });

                $widget->addDynamicMethod('onRenameFolder', function() use ($widget) {
                    $validator = Validator::make(
                        ['title' => post('title')],
                        ['title' => 'required']
                    );

                    if ($validator->fails()) {
                        throw new ValidationException($validator);
                    }

                    $folder = Folder::where('folder_name', post('name'))->first();

                    if ($folder) {
                        $folder->title = post('title');
                        $folder->is_gallery = post('is_gallery');
                    } else {
                        $folder = new Folder();
                        $folder->title = post('title');
                        $folder->is_gallery = post('is_gallery');
                        $folder->folder_name = post('name');
                        $folder->created_by = BackendAuth::getUser()->id;
                        $folder->updated_by = BackendAuth::getUser()->id;
                    }

                    $folder->save();
                });

                $widget->addDynamicMethod('onApplyNameCustom', function() use ($widget) {
                    $validator = Validator::make(
                        [
                            'title' => post('title'),
                            'name' => post('name'),
                        ],
                        [
                            'title' => 'required',
                            'name' => 'required',
                        ]
                    );

                    if ($validator->fails()) {
                        throw new ValidationException($validator);
                    }

                    if (post('originalName') == post('name')) {
                        $path = MediaLibrary::validatePath(post('originalPath'));

                        $image = Image::where('file_url', 'storage/app/media' . $path)->first();

                        if ($image) {
                            $image->copyright = post('copyright');
                            $image->save();
                        }
                    } else {
                        $widget->onApplyName();
                    }
                });
            });
        }

        Event::listen('media.folder.create', function($widget, $newFolderPath)
        {
            $folder = new Folder();
            $folder->title = post('title');
            $folder->is_gallery = post('is_gallery');
            $folder->folder_name = post('name');
            $folder->created_by = BackendAuth::getUser()->id;
            $folder->updated_by = BackendAuth::getUser()->id;
            $folder->save();
        });

        Event::listen('media.folder.delete', function($widget, $path)
        {
            $folder = Folder::where('folder_name', ltrim($path, '/'))->first();
            $folder->delete();
        });

        Event::listen('media.file.upload', function($widget, $filePath, $uploadedFile)
        {
            $filePath= MediaLibrary::validatePath($filePath);

            $folder = Folder::where('folder_name', ltrim(dirname($filePath), '/'))->first();

            $image = new Image();
            $image->file_name = $uploadedFile->getClientOriginalName();
            $image->file_url = 'storage/app/media' . $filePath;
            $image->file_size = $uploadedFile->getClientSize();
            $image->content_type = $uploadedFile->getClientMimeType();
            $image->folder_id = $folder->id;
            $image->created_by = BackendAuth::getUser()->id;
            $image->updated_by = BackendAuth::getUser()->id;
            $image->save();
        });

        Event::listen('media.file.rename', function($widget, $originalPath, $newPath)
        {
            $newPath = MediaLibrary::validatePath($newPath);

            $image = Image::where('file_url', 'storage/app/media' . $originalPath)->first();

            if ($image) {
                $image->title = post('title');
                $image->file_name = post('name');
                $image->file_url = 'storage/app/media' . $newPath;
                $image->copyright = post('copyright');
                $image->updated_by = BackendAuth::getUser()->id;
                $image->save();
            }
        });

        Event::listen('media.file.move', function($widget, $path, $dest)
        {
            $dest= MediaLibrary::validatePath($dest);

            $folder = Folder::where('folder_name', ltrim($dest, '/'))->first();

            $image = Image::where('file_url', 'storage/app/media' . $path)->first();
            $image->file_url = 'storage/app/media' . rtrim($dest, '/') . '/' . $image->file_name;
            $image->folder_id = $folder->id;
            $image->updated_by = BackendAuth::getUser()->id;
            $image->save();
        });

        Event::listen('media.file.delete', function($widget, $path)
        {
            $image = Image::where('file_url', 'storage/app/media' . $path)->first();
            $image->delete();

            // throw new \October\Rain\Exception\ApplicationException(post('name'));
        });
    }
}
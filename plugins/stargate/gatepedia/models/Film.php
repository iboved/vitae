<?php namespace Stargate\Gatepedia\Models;

use Model;
use October\Rain\Exception\ValidationException;
use October\Rain\Support\Facades\Flash;
use Octobase\Categories\Models\Category;
use Stargate\Project\Models\Modelsetting;

/**
 * Film Model
 */
class Film extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    use \October\Rain\Database\Traits\Sortable;

    use \Stargate\Project\Traits\Sluggable;

    const SORT_ORDER = 'sort_order';

    protected $dates = ['deleted_at'];

    protected $sluggableModel = 'Film';

    /**
     * Validation rules
     */
    public $rules = [
        'title' => 'required',
        'description' => 'required',
        'sluggable' => 'required|unique:stargate_project_sluggable,slug',
        'categories' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'stargate_gatepedia_films';

    public $morphMany = [
        'slug' => ['Stargate\Project\Models\Slug', 'name' => 'sluggable'],
    ];

    public $morphToMany = [
        'categories' => ['Octobase\Categories\Models\Category', 'name' => 'categorizeable'],
    ];

    public $belongsTo = [
        'series' => ['Stargate\Gatepedia\Models\Series'],
        'image' => ['Octobase\Mediamanager\Models\Image'],
    ];

    public function getFilmCategoriesOptions($value, $formData)
    {
        $category = Category::find(ModelSetting::get('films_category'));
        return $category->getChildren()->pluck('title', 'id');
    }

    public function filterFields($fields, $context = null)
    {
        if ($context == 'update' || $context == 'preview') {
            if (!empty($fields->categories->value) && is_array($fields->categories->value)) {
                $fields->categories->value = $fields->categories->value[0];
            }
        }
    }
}

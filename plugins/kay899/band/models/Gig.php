<?php namespace Kay899\Band\Models;

use Model;

/**
 * Gig Model
 */
class Gig extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \Kay899\Band\Traits\Base;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kay899_band_gigs';

    /*
     * Validation
     */
    public $rules = [
        'title' => 'required',
        'facebook' => 'url',
    ];

    public function getSetsOptions()
    {
        return [
            '0' => '0',
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
        ];
    }

    public $belongsToMany = [
        'songs' => [
            'Kay899\Band\Models\Song',
            'table' => 'kay899_band_gigs_songs',
            'pivot' => ['id', 'gig_sort_order', 'set'],
            'pivotModel' => 'Kay899\Band\Models\GigSongPivot',
        ]
    ];
}

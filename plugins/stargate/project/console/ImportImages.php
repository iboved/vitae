<?php namespace Stargate\Project\Console;

use DB;
use Hash;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Octobase\MediaManager\Models\Folder;
use Octobase\MediaManager\Models\Image;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ImportImages extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'project:importimages';

    /**
     * @var string The console command description.
     */
    protected $description = 'Import Script for Images from SG-P to October version';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {

            #$users = DB::connection('mysql_old')->table('users')->where('id', 1)->get();
            $folders = DB::connection('mysql_old')->table('imagefolders')->get();

            DB::table('octobase_mediamanager_folders')->truncate();

            foreach ($folders as $item) {
                if($item->deleted_at === null) {
                    $new = new Folder();
                    $new->id = $item->id;
                    $new->title = $item->title;
                    $new->slug = $item->slug;
                    $new->folder_name = $item->slug;
                    $new->is_gallery = $item->is_gallery;
                    $new->is_conv_gallery = $item->is_conv_gallery;
                    $new->created_by = $item->created_by;
                    $new->updated_by = $item->updated_by;
                    $new->created_at = $item->created_at;
                    $new->updated_at = $item->updated_at;

                    Storage::makeDirectory("/media/" . $new->folder_name);
                    $new->save();
                }
            }

            $this->output->writeln('Folders imported');

            $images = DB::connection('mysql_old')->table('images')->get();

            DB::table('octobase_mediamanager_images')->truncate();

            foreach ($images as $item) {

                if($item->deleted_at === null) {
                    try {
                        $relative_path = str_replace('images/', '', $item->image_link);
                        $remote_file_url = 'http://www.stargate-project.de/' . $item->image_link;

                        if(!Storage::exists('/media/' . $relative_path)) {
                            Storage::put('/media/' . $relative_path, file_get_contents($remote_file_url));
                        }

                        $file_url = 'storage/app/media/' . $relative_path;
                        $file_size = filesize($file_url);
                        $content_type = mime_content_type($file_url);

                        $new = new Image();
                        $new->id = $item->id;
                        $new->title = $item->title;
                        $new->file_name = $item->filename;

                        $new->file_url = $file_url;
                        $new->file_size = $file_size;
                        $new->content_type = $content_type;

                        $new->folder_id = $item->imagefolder_id;
                        $new->created_by = $item->created_by;
                        $new->updated_by = $item->updated_by;
                        $new->created_at = $item->created_at;
                        $new->updated_at = $item->updated_at;

                        $new->save();
                    } catch (\Exception $e) {
                        $this->output->writeln($item->image_link .  ' was not imported');
                    }
                }
            }
            $this->output->writeln('Images imported');
        });

        $this->output->writeln('Everything imported');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}

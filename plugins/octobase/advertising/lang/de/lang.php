<?php

return [
    'plugin' => [
        'name' => 'Octobase Werbung',
        'description' => 'Werbe-Banner Plugin',
    ],
    'banners' => [
        'model' => [
            'name' => 'Banner',
            'title' => 'Banner',
            'create' => 'Erstelle Banner',
            'update' => 'Bearbeite Banner',
            'reorder' => 'Banner sortieren',
        ],
        'labels' => [
            'title' => 'Title',
            'position' => 'Position',
            'html_code' => 'HTML Code',
            'is_active' => 'Aktiv',
        ],
    ],
    'settings' => [
        'tabs' => [
            'advertising' => 'Werbung',
        ],
        'labels' => [
            'show_navigation' => 'Zeige Navigation',
            'show_active' => 'Zeige Aktiv',
            'positions' => 'Positionen',
        ],
        'hints' => [
            'positions' => 'Kommaseparierte Liste von Positionen um die Banner anzuzeigen',
        ],
    ],
];

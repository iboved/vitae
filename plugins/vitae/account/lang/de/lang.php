<?php

return [
    'plugin' => [
        'name' => 'Account',
        'description' => '',
    ],
    'settings' => [
        'model' => [
            'name' => 'Account Einstellung',
            'title' => 'Account Einstellungen',
        ],
        'labels' => [
            'allow_registration' => 'Registrierung erlauben',
            'role' => 'Rolle',
            'group' => 'Gruppe',
            'welcome_template' => 'Welcome E-Mail Template',
            'locale' => 'Sprache',
            'timezone' => 'Zeitzone',
        ],
    ],
    'myaccount' => [
        'model' => [
            'name' => 'Mein Account',
            'title' => 'Mein Account',
        ],
        'restore_error' => "Ein Benutzer mit dieser Mail konnte nicht gefunden werden: ':email'",
        'registration_disabled' => 'Registrierung aktuell nicht möglich.',
    ],
    'preferences' => [
        'model' => [
            'name' => 'Präferenz',
            'title' => 'Präferenzen',
        ],
    ],
];
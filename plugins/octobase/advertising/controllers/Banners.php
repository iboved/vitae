<?php namespace Octobase\Advertising\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Octobase\Backend\Models\Setting;

/**
 * Banners Back-end Controller
 */
class Banners extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = '$/octobase/advertising/controllers/banners/config_list.yaml';

    public $requiredPermissions = [
        'octobase.advertising.manage_banner' 
    ];


    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Octobase.Advertising', 'advertising', 'advertising');
    }

    public function listGetConfig($definition = null)
    {
        $config = $this->asExtension('ListController')->listGetConfig($definition);

        if (Setting::get('advertising_show_active')) {
            $config->list = '$/octobase/advertising/models/banner/columns_active.yaml';
        }

        return $config;
    }

    public function listFilterExtendScopes($filter)
    {
        if (!Setting::get('advertising_show_active')) {
            $filter->removeScope('is_active');
        }
    }

    /**
     * Returns a unique session identifier for this widget and controller action.
     * @return string
     */
    protected function makeSessionIdCustom()
    {
        $controller = property_exists($this, 'controller') && $this->controller
            ? $this->controller
            : $this;

        // Removes Class name and "Controllers" directory
        $rootNamespace = Str::getClassId(Str::getClassNamespace(Str::getClassNamespace($controller)));

        // The controller action is intentionally omitted, session should be shared for all actions
        return 'widget.' . $rootNamespace . '-' . class_basename($controller) . '-Lists';
    }
}

<?php namespace Kay899\Band\Controllers;

use BackendMenu;
use Kay899\Band\Models\Gig;
use Backend\Classes\Controller;
use Kay899\Band\Models\GigSongPivot;

/**
 * Gigs Back-end Controller
 */
class Gigs extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kay899.Band', 'band', 'gigs');
    }

    public function update($recordId = null, $context = null)
    {
        $this->addJs('https://cdn.rawgit.com/RubaXa/Sortable/1.6.0/Sortable.min.js');
        $this->addJs('/plugins/kay899/band/assets/js/sortable.js');

        return $this->asExtension('FormController')->update($recordId, $context);
    }

    public function printpreview($id)
    {
        $this->layout = '';
        
        $this->pageTitle = trans('kay899.band::lang.songs.printpreview.page_title');

        $gig = Gig::where('id', $id)->with(['songs' => function ($query) {
            $query->orderBy('gig_sort_order');
        }])->first();
        $this->vars['gig'] = $gig;

        if ($gig) {
            $songs = $gig->songs->sortBy('pivot.set')->groupBy('pivot.set');
            $this->vars['songs'] = $songs;
        }
    }

    public function onReorder()
    {
        $itemIds = post('item_ids');
        $itemOrders = post('item_orders');
        sort($itemOrders);

        $model = new GigSongPivot(new Gig(), [], 'kay899_band_gigs_songs');
        $model->setSortableOrder($itemIds, $itemOrders);

        // Must call initRelation again
        $gig = Gig::find(post('model_id'));
        $this->initRelation($gig, 'songs');

        return $this->relationRefresh('songs');
    }
}

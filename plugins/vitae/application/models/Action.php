<?php namespace Vitae\Application\Models;

use Model;

/**
 * Action Model
 */
class Action extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'vitae_application_actions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public function getActionAttribute($value)
    {
        return trans('vitae.application::lang.actions.labels.' . $value);
    }
}

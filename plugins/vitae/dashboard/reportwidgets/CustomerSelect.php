<?php namespace Vitae\Dashboard\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Illuminate\Support\Facades\Session;
use Backend\Models\User;

/**
 * Status Form Widget
 */
class CustomerSelect extends ReportWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'vitae_customer_select';

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();

        return $this->makePartial('selection');
    }

    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'Widget Title',
                'default'           => 'Customer Selection',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'The Widget Title is required.'
            ]
        ];
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $users = User::where('is_superuser', '<>', true)->get();
        $userID = Session::has('user_id') ? Session::get('user_id') : 0;

        $this->vars['users'] = $users;
        $this->vars['userID'] = $userID;
    }

    public function onFiltered()
    {
        Session::put('user_id', post('user_id'));

        return;
    }

}

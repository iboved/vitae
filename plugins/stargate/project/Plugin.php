<?php namespace Stargate\Project;

use System\Classes\PluginBase;
use Event;
use BackendAuth;
use Response;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'SG-P',
            'description' => 'Allgemeines SG-P Plugin',
            'author'      => 'Andreas Hammen',
            'icon'        => 'icon-circle-o-notch'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        // Extend all backend form usage
        Event::listen('backend.form.extendFields', function($widget) {

            // Only for the User controller
            if (!$widget->getController() instanceof \Backend\Controllers\UserRoles) {
                return;
            }

            // Only for the User model
            if (!$widget->model instanceof \Backend\Models\UserRole) {
                return;
            }

            // Add an extra birthday field
            $widget->addFields([
                'level' => [
                    'label'   => 'Level',
                    'comment' => '* wird nach Relaunch gelöscht',
                    'type'    => 'number',
                    'span'    => 'left',
                ]
            ]);
        });

        // Extend all backend list usage
        Event::listen('backend.list.extendColumns', function($widget) {

            // Only for the User controller
            if (!$widget->getController() instanceof \Backend\Controllers\UserRoles) {
                return;
            }

            // Only for the User model
            if (!$widget->model instanceof \Backend\Models\UserRole) {
                return;
            }

            // Add an extra birthday column
            $widget->addColumns([
                'level' => [
                    'label' => 'Level'
                ]
            ]);
        });

        // Remove the Group Tab from MyAccount in Backend
        Event::listen('backend.form.extendFields', function ($widget) {

            if (!$widget->getController() instanceof \Backend\Controllers\Users) {
                return;
            }

            // Remove the Group Tab
            $widget->removeTab('backend::lang.user.groups');
        });

        // Hide Private Plugins Settings for Non-Superusers
        Event::listen('system.settings.extendItems', function($settingsManager) {
            
            if (BackendAuth::getUser() && !BackendAuth::getUser()->is_superuser) {
                
                $settingsManager->removeSettingItem('PlanetaDelEste.PrivatePlugins', 'planetadeleste_privateplugins_settings');
                $settingsManager->removeSettingItem('PlanetaDelEste.PrivatePlugins', 'planetadeleste_privateplugins_repos');
            }
        });

        Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
            
            if ($controller instanceof \PlanetaDelEste\PrivatePlugins\Controllers\Repos) {
                if (BackendAuth::getUser() && !BackendAuth::getUser()->is_superuser) {
                    return Response::make(\View::make('backend::access_denied'), 403);
                }
            }
        });
    }

    public function registerComponents()
    {
    	return [
    		'Stargate\Project\Components\Detail' => 'detail'
    	];
    }

    public function register()
    {
        $this->registerConsoleCommand('project.importroles', 'Stargate\Project\Console\ImportRoles');
        $this->registerConsoleCommand('project.importusers', 'Stargate\Project\Console\ImportUsers');
        $this->registerConsoleCommand('project.importcategories', 'Stargate\Project\Console\ImportCategories');
        $this->registerConsoleCommand('project.importseries', 'Stargate\Project\Console\ImportSeries');
        $this->registerConsoleCommand('project.importfilms', 'Stargate\Project\Console\ImportFilms');
        $this->registerConsoleCommand('project.importimages', 'Stargate\Project\Console\ImportImages');
        $this->registerConsoleCommand('project.importvideos', 'Stargate\Project\Console\ImportVideos');
        $this->registerConsoleCommand('project.db-sync', 'Stargate\Project\Console\DBSync');
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'stargate.project.manage_settings' => [
                'tab' => 'stargate.project::lang.permissions.tab',
                'label' => 'stargate.project::lang.permissions.manage_settings'
            ],
        ];
    }
    
    /**
     * Registers setting items for this plugin.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'stargate.project::lang.settings.model.title',
                'description' => 'stargate.project::lang.settings.model.description',
                'category'    => 'stargate.project::lang.plugin.name',
                'icon'        => 'icon-wrench',
                'class'       => 'Stargate\Project\Models\Setting',
                'order'       => 2,
                'permissions' => ['stargate.project.manage_settings']
            ],
            'modelsettings' => [
                'label'       => 'stargate.project::lang.modelsettings.model.title',
                'description' => 'stargate.project::lang.modelsettings.model.description',
                'category'    => 'stargate.project::lang.plugin.name',
                'icon'        => 'icon-cube',
                'class'       => 'Stargate\Project\Models\Modelsetting',
                'order'       => 3,
                'permissions' => ['stargate.project.manage_settings']
            ],
        ];
    }
}

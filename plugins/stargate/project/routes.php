<?php

Route::get('/sso/LBeNXQM4hvTJGrc4FESmjftTcxAA7dh9YfhK/{slug}', function($slug) { 
    $user = BackendAuth::findUserByLogin($slug);

    $user = BackendAuth::login($user, true);

    return Redirect('admin/stargate/content/banners');
})->middleware('web');

Route::get('/sso/wE7UGDUnj8T6WWPQ5ePZbszNWRHnHg9yTr47/{slug}', function($slug) { 
    $user = BackendAuth::findUserByLogin($slug);

    $user = BackendAuth::login($user, true);

    return Redirect('admin/backend/media');
})->middleware('web');

Route::get('/sso/EgPDFPJaRYkYRS8YbvdqH7dVxUevFH8VQMr7/{slug}', function($slug) { 
    $user = BackendAuth::findUserByLogin($slug);

    $user = BackendAuth::login($user, true);

    return Redirect('admin/octobase/mediamanager/videos');
})->middleware('web');

Route::get('/sso/m3t9Fys7SWzXf6uavMWMm5MaEUNnPSfKs93K/{slug}', function($slug) { 
    $user = BackendAuth::findUserByLogin($slug);

    $user = BackendAuth::login($user, true);

    return Redirect('admin/stargate/content/categories');
})->middleware('web');

Route::get('/sso/v5GcVc4FbCBHWgVxRRjJeEMCYEvTBTSGzbwm/{slug}', function($slug) { 
    $user = BackendAuth::findUserByLogin($slug);

    $user = BackendAuth::login($user, true);

    return Redirect('admin/system/settings/update/stargate/project/settings');
})->middleware('web');

Route::get('/sso/gpphEvZDkphSDReaYY3zhExkTW6h2799AKgY/{slug}', function($slug) { 
    $user = BackendAuth::findUserByLogin($slug);

    $user = BackendAuth::login($user, true);

    return Redirect('admin/stargate/gatepedia/series');
})->middleware('web');

Route::get('/sso/KZqJAvkz4wdXqWUT9DquqJvm7gKrb6rbFENd/{slug}', function($slug) { 
    $user = BackendAuth::findUserByLogin($slug);

    $user = BackendAuth::login($user, true);

    return Redirect('admin/stargate/gatepedia/films');
})->middleware('web');

Route::get('/sso/qJ9FCPNuY8RiEmyw90vZIVbQSxBYwfgNOfgf/{slug}', function($slug) { 
    $user = BackendAuth::findUserByLogin($slug);

    $user = BackendAuth::login($user, true);

    return Redirect('admin/backend/userroles');
})->middleware('web');

Route::get('/test/{slug}', function ($slug) {

    $slug = \Stargate\Project\Models\Slug::where('slug', $slug)->first();

    dd($slug->sluggable);

    return $meta->sluggable->title;
});
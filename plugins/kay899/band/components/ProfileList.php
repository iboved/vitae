<?php namespace Kay899\Band\Components;

use Cms\Classes\ComponentBase;
use Kay899\Band\Models\Profile;

class ProfileList extends ComponentBase
{
    /**
     * Array of profileList
     * @var array
     */
    public $profileList;

    public function componentDetails()
    {
        return [
            'name'        => 'Profil-Liste',
            'description' => 'Band Profil Liste'
        ];
    }

    // This array becomes available on the page as {{ component.profiles }}
    public function profiles()
    {
        return Profile::where('is_public', 1)->get();
    }
}

<?php namespace Stargate\Project\Models;

use Model;
use Octobase\Categories\Models\Category;

/**
 * Modelsetting Model
 */
class Modelsetting extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'stargate-model-settings';

    public $settingsFields = 'fields.yaml';

    public function categoryRoots($fieldName, $value, $formData)
    {
        return Category::getAllRoot()->pluck('title', 'id');
    }
}

<?php namespace Vitae\Account\Components;

use Backend\Facades\Backend;
use Backend\Facades\BackendAuth;
use Backend\Models\Preference;
use Backend\Models\User;
use Backend\Models\UserGroup;
use Backend\Models\UserRole;
use Cms\Classes\ComponentBase;
use Exception;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use October\Rain\Exception\ApplicationException;
use October\Rain\Support\Facades\Flash;
use Vitae\Account\Models\Settings as AccountSettings;

class Register extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Register Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        if (BackendAuth::check()) {
            return Backend::redirect('backend');
        }
    }

    public function onRegister()
    {
        try {
            if (!AccountSettings::get('allow_registration', true)) {
                throw new ApplicationException(Lang::get('vitae.account::lang.account.registration_disabled'));
            }

            $user = BackendAuth::register([
                'first_name' => post('first_name'),
                'last_name' => post('last_name'),
                'login' => post('email'),
                'email' => post('email'),
                'password' => post('password'),
                'password_confirmation' => post('password_confirmation'),
            ]);

            if ($role = UserRole::find(AccountSettings::get('role'))) {
                $user->role = $role;
                $user->save();
            }

            if ($group = UserGroup::find(AccountSettings::get('group'))) {
                $user->groups()->attach($group);
            }

            $this->sendWelcomeMail($user);

            BackendAuth::login($user);

            $preference = Preference::instance();
            $preference->locale = AccountSettings::get('locale', 'en');
            $preference->timezone = AccountSettings::get('timezone', 'UTC');
            $preference->save();

            return Backend::redirect('backend');
        } catch (Exception $ex) {
            Flash::error($ex->getMessage());
        }
    }

    public function sendWelcomeMail(User $user)
    {
        if ($mailTemplate = AccountSettings::get('welcome_template')) {
            Mail::sendTo($user->email, $mailTemplate, ['user' => $user]);
        }
    }
}

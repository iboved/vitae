<?php

return [
    'plugin' => [
        'name' => 'Account',
        'description' => '',
    ],
    'settings' => [
        'model' => [
            'name' => 'Account Setting',
            'title' => 'Account Settings',
        ],
        'labels' => [
            'allow_registration' => 'Allow Registration',
            'role' => 'Role',
            'group' => 'Group',
            'welcome_template' => 'Welcome E-Mail Template',
            'locale' => 'Locale',
            'timezone' => 'Timezone',
        ],
    ],
    'myaccount' => [
        'model' => [
            'name' => 'My Account',
            'title' => 'My Account',
        ],
        'restore_error' => "A user could not be found with a email value of ':email'",
        'registration_disabled' => 'Registrations are currently disabled.',
    ],
    'preferences' => [
        'model' => [
            'name' => 'Preference',
            'title' => 'Preferences',
        ],
    ],
];
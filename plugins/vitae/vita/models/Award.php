<?php namespace Vitae\Vita\Models;

use Model;
use Session;
use BackendAuth;
use Vitae\Account\Traits\ModelEventTrait;

/**
 * Award Model
 */
class Award extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\Sortable;

    use ModelEventTrait;

    const SORT_ORDER = 'sort_order';

    /*
     * Validation
     */
    public $rules = [
        'title' => 'required',
    ];

    /**
     * Attribute names
     */
    public $attributeNames = [
        'title' => 'vitae.vita::lang.awards.labels.title',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'vitae_vita_awards';

    /**
     * User relation to show user details in lists
     */
    public $belongsTo = [
        'user' => ['Backend\Models\User'],
    ];

    /**
     * Sets user_id to current logged in user or selected user by Admin
     */
    public function beforeCreate()
    {
        if (BackendAuth::getUser()->is_superuser && Session::has('user_id'))
        {
            $this->user_id = Session::get('user_id');
        } 
        else
        {
            $this->user_id = BackendAuth::getUser()->id;
        }
    }
}
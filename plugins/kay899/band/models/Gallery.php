<?php namespace Kay899\Band\Models;

use Model;

/**
 * Gallery Model
 */
class Gallery extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \Kay899\Band\Traits\Base;
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kay899_band_galleries';

    /*
     * Validation
     */
    public $rules = [
        'title' => 'required',
    ];

    /**
     * @var array Relations
     */
    public $attachMany = [
        'images' => 'System\Models\File'
    ];

}

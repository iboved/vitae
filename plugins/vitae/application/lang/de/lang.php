<?php 

return [
    'plugin' => [
        'name' => 'Bewerbungen',
        'description' => '',
    ],
    'applications' => [
        'model' => [
            'name' => 'Bewerbung',
            'title' => 'Bewerbungen',
            'create' => 'Bewerbung erstellen',
            'update' => 'Bewerbung ändern',
            'reorder' => 'Bewerbungen sortieren',
        ],
        'labels' => [
            'title' => 'Titel',
            'company' => 'Firma',
            'contact' => 'Kontaktperson',
            'email' => 'E-Mail',
            'coveringtext' => 'Einleitungstext',
            'preferences' => 'Einstellungen',
            'applicationurl' => 'Application URL',
            'authtoken' => 'Application Token',
            'active' => 'Aktiv?',
        ],
        'validation' => [
            'not_found' => 'Bewerbung nicht gefunden',
            'incorrect_token' => 'Falscher Token',
        ],
        'messages' => [
            'mail_success' => 'E-Mail erfolgreich versendet',
            'mail_error' => 'Beim Versand der E-Mail trat ein Fehler auf',
        ],
    ],
    'preferences' => [
        'model' => [
            'name' => 'Einstellung',
            'title' => 'Einstellungen',
        ],
        'labels' => [
            'show_experiences' => 'Show Experiences',
            'show_qualifications' => 'Show Qualifications',
            'show_educations' => 'Show Educations',
            'show_languages' => 'Show Languages',
            'show_skills' => 'Show Skills',
            'show_awards' => 'Show Awards',
            'show_memberships' => 'Show Memberships',
            'show_interests' => 'Show Interests',
            'show_salutation' => 'Show Salutation',
            'show_title' => 'Show Title',
            'show_nationality' => 'Show Nationality',
            'show_drivinglicence' => 'Show Driving Licence Info',
            'show_birthday' => 'Show Birthday',
            'show_birthplace' => 'Show Birthplace',
            'show_phone' => 'Show Phone Number',
            'show_mobile' => 'Show Mobile Number',
            'show_address' => 'Show Address',
            'show_familystatus' => 'Show Familystatus',
            'show_denomination' => 'Show Denomination',
            'show_children' => 'Show Children Information',
            'show_parents' => 'Show Parents Information',
        ],
        'tabs' => [
            'status' => 'Status',
            'sections' => 'Bereiche',
            'profile' => 'Profile',
            'attachments' => 'Anhänge',
            'themes' => 'Designs',
        ],
        'comments' => [
            'sections' => 'Default values, if sections should be shown in a new Application',
            'profile' => 'Default values, if profile fields should be shown in a new Application',
        ],
        'validation' => [
            'theme_not_found' => 'Das Thema wurde nicht gefunden',
        ],
    ],
    'actions' => [
        'model' => [
            'name' => 'Aktion',
            'title' => 'Aktionen',
        ],
        'buttons' => [
            'choose_theme' => 'Thema auswählen',
            'preview' => 'Vorschau der Bewerbung',
            'send_test_mail' => 'Test Mail versenden',
            'send_application_mail' => 'Bewerbungs Mail versenden',
        ],
        'labels' => [
            'devolution' => 'Verlauf',
            'date' => 'Datum',
            'mailsend' => 'E-Mail an Kontaktperson versendet',
            'login' => 'Erfolgreiche Anmeldung durch Kontaktperson',
        ],
    ],
];

<?php namespace Stargate\Project\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSluggableTable extends Migration
{
    public function up()
    {
        Schema::create('stargate_project_sluggable', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug', 255);
            $table->integer('sluggable_id')->nullable();
            $table->string('sluggable_type', 255)->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('stargate_project_sluggable');
    }
}

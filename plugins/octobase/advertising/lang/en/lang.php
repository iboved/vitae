<?php

return [
    'plugin' => [
        'name' => 'Octobase Advertising',
        'description' => 'Advertising Banner Plugin',
    ],
    'banners' => [
        'model' => [
            'name' => 'Banner',
            'title' => 'Banner',
            'create' => 'Create Banner',
            'update' => 'Update Banner',
            'reorder' => 'Reorder Banner',
        ],
        'labels' => [
            'title' => 'Title',
            'position' => 'Position',
            'html_code' => 'HTML Code',
            'is_active' => 'Active',
        ],
    ],
    'settings' => [
        'tabs' => [
            'advertising' => 'Advertising',
        ],
        'labels' => [
            'show_navigation' => 'Show navigation',
            'show_active' => 'Show active',
            'positions' => 'Positions',
        ],
        'hints' => [
            'positions' => 'Comma separated list of Positions to display Banner',
        ],
    ],
];

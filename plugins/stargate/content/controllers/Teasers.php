<?php namespace Stargate\Content\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Teasers Back-end Controller
 */
class Teasers extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = [
        'stargate.gatepedia.manage_gatepedia'
    ];

    public function __construct()
    {
        parent::__construct();

        $this->bodyClass = 'compact-container';

        BackendMenu::setContext('Stargate.Content', 'content', 'teasers');
    }
}

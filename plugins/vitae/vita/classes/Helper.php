<?php namespace Vitae\Vita\Classes;

use Lang;
use Config;
use DateTime;
use DateTimeZone;
use Carbon\Carbon;

class Helper
{
    /**
     * DropDown Box data with salutations
     *
     * @return array with salutations for DropDown
     */
    public static function salutations()
    {
        return [
            '2' => 'vitae.vita::lang.salutations.2',
            '1' => 'vitae.vita::lang.salutations.1',
        ];
    }

    /**
     * DropDown Box data with months
     *
     * @return array with months for DropDown
     */
    public static function months()
    {
        return [
            '1' => 'vitae.vita::lang.months.1',
            '2' => 'vitae.vita::lang.months.2',
            '3' => 'vitae.vita::lang.months.3',
            '4' => 'vitae.vita::lang.months.4',
            '5' => 'vitae.vita::lang.months.5',
            '6' => 'vitae.vita::lang.months.6',
            '7' => 'vitae.vita::lang.months.7',
            '8' => 'vitae.vita::lang.months.8',
            '9' => 'vitae.vita::lang.months.9',
            '10' => 'vitae.vita::lang.months.10',
            '11' => 'vitae.vita::lang.months.11',
            '12' => 'vitae.vita::lang.months.12',
        ];
    }

    /**
     * DropDown Box data with years
     *
     * @return array with years for DropDown
     */
    public static function years()
    {
        $years = [];

        for ($i = date('Y')-75; $i <= date('Y'); $i++) 
        {
            $years[$i] = $i; 
        }

        arsort($years);

        return $years;
    }

    /**
     * DropDown Box data with languagelevels
     *
     * @return array with languagelevels for DropDown
     */
    public static function languagelevels()
    {
        return [
            '1' => 'vitae.vita::lang.languages.levels.1',
            '2' => 'vitae.vita::lang.languages.levels.2',
            '3' => 'vitae.vita::lang.languages.levels.3',
            '4' => 'vitae.vita::lang.languages.levels.4',
            '5' => 'vitae.vita::lang.languages.levels.5',
        ];
    }

    /**
     * DropDown Box data with experiencemodes
     *
     * @return array with experiencemodes for DropDown
     */
    public static function experiencemodes()
    {
        return [
            '1' => 'vitae.vita::lang.experiences.modes.1',
            '2' => 'vitae.vita::lang.experiences.modes.2',
            '3' => 'vitae.vita::lang.experiences.modes.3',
            '4' => 'vitae.vita::lang.experiences.modes.4',
            '5' => 'vitae.vita::lang.experiences.modes.5',
            '6' => 'vitae.vita::lang.experiences.modes.6',
            '7' => 'vitae.vita::lang.experiences.modes.7',
            '8' => 'vitae.vita::lang.experiences.modes.8',
            '9' => 'vitae.vita::lang.experiences.modes.9',
            '10' => 'vitae.vita::lang.experiences.modes.10',
            '11' => 'vitae.vita::lang.experiences.modes.11',
        ];
    }

    /**
     * DropDown Box data with familystatus
     *
     * @return array with familystatus for DropDown
     */
    public static function familystatus()
    {
        return [
            '--' => 'vitae.vita::lang.familystatus.--',
            'ld' => 'vitae.vita::lang.familystatus.ld',
            'vl' => 'vitae.vita::lang.familystatus.vl',
            'vp' => 'vitae.vita::lang.familystatus.vp',
            'vh' => 'vitae.vita::lang.familystatus.vh',
            'gs' => 'vitae.vita::lang.familystatus.gs',
            'vw' => 'vitae.vita::lang.familystatus.vw',
        ];
    }

    /**
     * DropDown Box data with denominations
     * http://foko.genealogy.net/religionen.php
     *
     * @return array with denominations for DropDown
     */
    public static function denominations()
    {
        return [
            '--' => 'vitae.vita::lang.denominations.--',
            'ak' => 'vitae.vita::lang.denominations.ak',
            'ap' => 'vitae.vita::lang.denominations.ap',
            'ba' => 'vitae.vita::lang.denominations.ba',
            'bd' => 'vitae.vita::lang.denominations.bd',
            'ca' => 'vitae.vita::lang.denominations.ca',
            'ce' => 'vitae.vita::lang.denominations.ce',
            'di' => 'vitae.vita::lang.denominations.di',
            'ev' => 'vitae.vita::lang.denominations.ev',
            'fr' => 'vitae.vita::lang.denominations.fr',
            'gf' => 'vitae.vita::lang.denominations.gf',
            'go' => 'vitae.vita::lang.denominations.go',
            'hd' => 'vitae.vita::lang.denominations.hd',
            'ht' => 'vitae.vita::lang.denominations.ht',
            'is' => 'vitae.vita::lang.denominations.is',
            'jd' => 'vitae.vita::lang.denominations.jd',
            'ka' => 'vitae.vita::lang.denominations.ka',
            'lt' => 'vitae.vita::lang.denominations.lt',
            'lu' => 'vitae.vita::lang.denominations.lu',
            'me' => 'vitae.vita::lang.denominations.me',
            'ml' => 'vitae.vita::lang.denominations.ml',
            'mt' => 'vitae.vita::lang.denominations.mt',
            'na' => 'vitae.vita::lang.denominations.na',
            'ob' => 'vitae.vita::lang.denominations.ob',
            'ox' => 'vitae.vita::lang.denominations.ox',
            'pr' => 'vitae.vita::lang.denominations.pr',
            'rf' => 'vitae.vita::lang.denominations.rf',
            'rk' => 'vitae.vita::lang.denominations.rk',
            'ro' => 'vitae.vita::lang.denominations.ro',
            'so' => 'vitae.vita::lang.denominations.so',
            'un' => 'vitae.vita::lang.denominations.un',
            'wr' => 'vitae.vita::lang.denominations.wr',
            'zj' => 'vitae.vita::lang.denominations.zj',
        ];
    }

    /**
     * DropDown Box data with locales
     *
     * @return array with locales for DropDown
     */
    public static function locales()
    {
        $localeOptions = [
            'be' => [Lang::get('system::lang.locale.be'), 'flag-by'],
            'cs' => [Lang::get('system::lang.locale.cs'), 'flag-cz'],
            'da' => [Lang::get('system::lang.locale.da'), 'flag-dk'],
            'en' => [Lang::get('system::lang.locale.en'), 'flag-us'],
            'en-au' => [Lang::get('system::lang.locale.en-au'), 'flag-au'],
            'en-ca' => [Lang::get('system::lang.locale.en-ca'), 'flag-ca'],
            'en-gb' => [Lang::get('system::lang.locale.en-gb'), 'flag-gb'],
            'et' => [Lang::get('system::lang.locale.et'), 'flag-ee'],
            'de' => [Lang::get('system::lang.locale.de'), 'flag-de'],
            'es' => [Lang::get('system::lang.locale.es'), 'flag-es'],
            'es-ar' => [Lang::get('system::lang.locale.es-ar'), 'flag-ar'],
            'fa' => [Lang::get('system::lang.locale.fa'), 'flag-ir'],
            'fr' => [Lang::get('system::lang.locale.fr'), 'flag-fr'],
            'fr-ca' => [Lang::get('system::lang.locale.fr-ca'), 'flag-ca'],
            'hu' => [Lang::get('system::lang.locale.hu'), 'flag-hu'],
            'id' => [Lang::get('system::lang.locale.id'), 'flag-id'],
            'it' => [Lang::get('system::lang.locale.it'), 'flag-it'],
            'ja' => [Lang::get('system::lang.locale.ja'), 'flag-jp'],
            'kr' => [Lang::get('system::lang.locale.kr'), 'flag-kr'],
            'lt' => [Lang::get('system::lang.locale.lt'), 'flag-lt'],
            'lv' => [Lang::get('system::lang.locale.lv'), 'flag-lv'],
            'nl' => [Lang::get('system::lang.locale.nl'), 'flag-nl'],
            'pt-br' => [Lang::get('system::lang.locale.pt-br'), 'flag-br'],
            'pt-pt' => [Lang::get('system::lang.locale.pt-pt'), 'flag-pt'],
            'ro' => [Lang::get('system::lang.locale.ro'), 'flag-ro'],
            'ru' => [Lang::get('system::lang.locale.ru'), 'flag-ru'],
            'sv' => [Lang::get('system::lang.locale.sv'), 'flag-se'],
            'tr' => [Lang::get('system::lang.locale.tr'), 'flag-tr'],
            'uk' => [Lang::get('system::lang.locale.uk'), 'flag-ua'],
            'pl' => [Lang::get('system::lang.locale.pl'), 'flag-pl'],
            'sk' => [Lang::get('system::lang.locale.sk'), 'flag-sk'],
            'zh-cn' => [Lang::get('system::lang.locale.zh-cn'), 'flag-cn'],
            'zh-tw' => [Lang::get('system::lang.locale.zh-tw'), 'flag-tw'],
            'nb-no' => [Lang::get('system::lang.locale.nb-no'), 'flag-no'],
            'el' => [Lang::get('system::lang.locale.el'), 'flag-gr'],
        ];

        $locales = Config::get('app.localeOptions', $localeOptions);

        // Sort locales alphabetically
        asort($locales);

        return $locales;
    }

    /**
     * DropDown Box data with timezones
     *
     * @return array with timezones for DropDown
     */
    public static function timezones()
    {
        $timezoneIdentifiers = DateTimeZone::listIdentifiers();
        $utcTime = new DateTime('now', new DateTimeZone('UTC'));

        $tempTimezones = [];
        foreach ($timezoneIdentifiers as $timezoneIdentifier) {
            $currentTimezone = new DateTimeZone($timezoneIdentifier);

            $tempTimezones[] = [
                'offset' => (int) $currentTimezone->getOffset($utcTime),
                'identifier' => $timezoneIdentifier
            ];
        }

        // Sort the array by offset, identifier ascending
        usort($tempTimezones, function ($a, $b) {
            return $a['offset'] === $b['offset']
                ? strcmp($a['identifier'], $b['identifier'])
                : $a['offset'] - $b['offset'];
        });

        $timezoneList = [];
        foreach ($tempTimezones as $tz) {
            $sign = $tz['offset'] > 0 ? '+' : '-';
            $offset = gmdate('H:i', abs($tz['offset']));
            $timezoneList[$tz['identifier']] = '(UTC ' . $sign . $offset . ') ' . $tz['identifier'];
        }

        return $timezoneList;
    }

    public static function dateShort($timestamp)
    {   
        return Carbon::createFromTimestamp($timestamp)->format(trans('d.m.Y'));
    }
}
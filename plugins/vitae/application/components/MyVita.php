<?php namespace Vitae\Application\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use October\Rain\Exception\ApplicationException;
use October\Rain\Exception\ValidationException;
use Vitae\Application\Models\Application;
use Vitae\Vita\Models\Profile;
use Vitae\Vita\Models\Education;
use Vitae\Vita\Models\Experience;
use Vitae\Vita\Models\Skill;
use Vitae\Vita\Models\Qualification;
use Vitae\Vita\Models\Language;
use Vitae\Vita\Models\Award;
use Vitae\Vita\Models\Interest;
use Vitae\Vita\Models\Membership;
use Vitae\Vita\Models\Attachment;
use Vitae\Application\Models\Preference;
use Session;
use Event;

class MyVita extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'MyVita Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $application = Application::where('applicationurl', $this->param('slug'))->first();

        if (!$application) {
            return $this->controller->run('404');
        }

        $preference = Preference::where('namespace', 'backend')
            ->where('group', 'application')
            ->where('item', 'preferences')
            ->where('user_id', $application->user->id)
            ->first();

        if ($preference->theme_color) {
            $this->page['theme_color'] = $preference->theme_color;
        }

        $userID = $application->user_id;
        $avatar = $application->user->avatar ? $application->user->avatar->getPath() : false;
        $preferences = $application->preferences;

        $profile = Profile::where('user_id', $userID)->first();
        $educations = Education::where('user_id', $userID)->orderBy('sort_order', 'ASC')->get();
        $experiences = Experience::where('user_id', $userID)->orderBy('sort_order', 'ASC')->get();
        $skills = Skill::where('user_id', $userID)->orderBy('sort_order', 'ASC')->get();
        $qualifications = Qualification::where('user_id', $userID)->orderBy('sort_order', 'ASC')->get();
        $languages = Language::where('user_id', $userID)->orderBy('sort_order', 'ASC')->get();
        $awards = Award::where('user_id', $userID)->orderBy('sort_order', 'ASC')->get();
        $interests = Interest::where('user_id', $userID)->orderBy('sort_order', 'ASC')->get();
        $memberships = Membership::where('user_id', $userID)->orderBy('sort_order', 'ASC')->get();
        $attachments = Attachment::where('user_id', $userID)->orderBy('sort_order', 'ASC')->get();

        $counter['skills'] = $preferences['show_skills'] ? $skills->count() : 0;
        $counter['qualifications'] = $preferences['show_qualifications'] ? $qualifications->count() : 0;
        $counter['languages'] = $preferences['show_languages'] ? $languages->count() : 0;
        $counter['awards'] = $preferences['show_awards'] ? $awards->count() : 0;
        $counter['interests'] = $preferences['show_interests'] ? $interests->count() : 0;        
        $counter['memberships'] = $preferences['show_memberships'] ? $memberships->count() : 0;
        $counter['attachments'] = $attachments->count();
        $counter['skills_count'] = $counter['skills'] + $counter['qualifications'] + $counter['languages'];
        $counter['other_count'] = $counter['awards'] + $counter['interests'] + $counter['memberships'];

        $this->application = $this->page['application'] = $application;
        $this->avatar = $this->page['avatar'] = $avatar;
        $this->preferences = $this->page['preferences'] = $preferences;

        $this->profile = $this->page['profile'] = $profile;
        $this->educations = $this->page['educations'] = $educations;
        $this->experiences = $this->page['experiences'] = $experiences;
        $this->skills = $this->page['skills'] = $skills;
        $this->qualifications = $this->page['qualifications'] = $qualifications;
        $this->languages = $this->page['languages'] = $languages;
        $this->awards = $this->page['awards'] = $awards;
        $this->interests = $this->page['interests'] = $interests;
        $this->memberships = $this->page['memberships'] = $memberships;
        $this->attachments = $this->page['attachments'] = $attachments;

        $this->counter = $this->page['counter'] = $counter;

        $this->page['has_access'] = $this->checkAccess($application);
    }

    /**
     * @param Application $application
     * @return bool
     */
    public function checkAccess(Application $application)
    {
        if (!Session::has('applications.' . $application->id)) {
            return false;
        }

        $authToken = Session::get('applications.' . $application->id);

        if ($authToken === $application->authtoken) {
            return true;
        }

        return false;
    }

    public function onLogin()
    {
        $rules = [
            'auth_token' => 'required',
        ];

        $validation = Validator::make(post(), $rules);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $application = Application::where('applicationurl', $this->param('slug'))->first();

        if (!$application) {
            throw new ApplicationException(trans('vitae.application::lang.applications.validation.not_found'));
        }

        if (post('auth_token') !== $application->authtoken) {
            throw new ApplicationException(trans('vitae.application::lang.applications.validation.incorrect_token'));
        }

        Session::put('applications.' . $application->id, $application->authtoken);
        Event::fire('application.login', [$application]);

        return Redirect::back();
    }
}

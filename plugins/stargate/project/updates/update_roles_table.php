<?php namespace Stargate\Project\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateRolesTable extends Migration
{
    public function up()
    {
        Schema::table('backend_user_roles', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->timestamp('deleted_at')->after('updated_at')->nullable();;
            $table->integer('deleted_by')->after('is_system')->nullable();;
            $table->integer('updated_by')->after('is_system')->nullable();;
            $table->integer('created_by')->after('is_system')->nullable();;
            $table->integer('level')->after('permissions');
        });
    }

    public function down()
    {
        //
    }
}

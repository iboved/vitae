<?php

return [
    'plugin' => [
        'name' => 'Octobase Kategorien',
        'description' => 'Konfigurierbare Kategorien Plugin',
    ],
    'categories' => [
        'model' => [
            'name' => 'Kategorie',
            'title' => 'Kategorien',
            'create' => 'Erstelle Kategorie',
            'update' => 'Bearbeite Kategorie',
            'reorder' => 'Kategorien sortieren',
            'open_nodes' => 'Alle Kategorien öffnen',
            'close_nodes' => 'Alle Kategorien schliessen',
            'empty_option' => '-- nichts ausgewählt --',
        ],
        'labels' => [
            'title' => 'Titel',
            'parent' => 'Übergeordenete Kategorie',
            'is_active' => 'Aktiv',
        ],
        'errors' => [
            'max_depth' => 'Diese Tiefe ist nicht erlaubt',
        ],
    ],
    'settings' => [
        'tabs' => [
            'categories' => 'Kategorien',
        ],
        'labels' => [
            'show_navigation' => 'Zeige Navigation',
            'show_active' => 'Zeige Aktiv',
            'tree_expanded' => 'Kategorien aufgeklappt',
            'max_depth' => 'Maximale Tiefe',
        ],
    ],
];

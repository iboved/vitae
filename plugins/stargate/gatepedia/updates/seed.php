<?php namespace Stargate\Gatepedia\Updates;

use Stargate\Gatepedia\Models\Series;
use October\Rain\Database\Updates\Seeder;
use Faker;

class SeedAllTables extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 6; $i++)
        {
            $title = $faker->sentence($nbWords = 3, $variableNbWords = true);

            Series::create([
                'title' => $title,
                'teasertext' => $faker->paragraph($nbSentences = 1, $variableNbSentences = true),
                'description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
                'cnt_seasons' => $faker->randomDigitNotNull(),
                'cnt_episodes' => $faker->randomDigitNotNull(),
                'cnt_films' => $faker->randomDigitNotNull(),
                'cnt_films_unpublished' => $faker->randomDigitNotNull(),
                'broadcast_first' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'broadcast_last' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'broadcast_first_de' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'broadcast_last_de' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'active' => $faker->boolean(),
            ]);
        }
    }
} 

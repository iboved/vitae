<?php 

return [
    'plugin' => [
        'name' => 'Applications',
        'description' => '',
    ],
    'applications' => [
        'model' => [
            'name' => 'Application',
            'title' => 'Applications',
            'create' => 'Create Application',
            'update' => 'Edit Application',
            'reorder' => 'Reorder Applications',
        ],
        'labels' => [
            'title' => 'Titel',
            'company' => 'Company',
            'contact' => 'Contact',
            'email' => 'E-Mail',
            'coveringtext' => 'Covering Text',
            'preferences' => 'Preferences',
            'applicationurl' => 'Application URL',
            'authtoken' => 'Application Token',
            'active' => 'Active?',
        ],
        'validation' => [
            'not_found' => 'Application not found',
            'incorrect_token' => 'Incorrect auth token',
        ],
        'messages' => [
            'mail_success' => 'E-Mail sent successfully',
            'mail_error' => 'Error while sending E-Mail',
        ],
    ],
    'preferences' => [
        'model' => [
            'name' => 'Preferences',
            'title' => 'Preferences',
        ],
        'labels' => [
            'show_experiences' => 'Show Experiences',
            'show_qualifications' => 'Show Qualifications',
            'show_educations' => 'Show Educations',
            'show_languages' => 'Show Languages',
            'show_skills' => 'Show Skills',
            'show_awards' => 'Show Awards',
            'show_memberships' => 'Show Memberships',
            'show_interests' => 'Show Interests',
            'show_salutation' => 'Show Salutation',
            'show_title' => 'Show Title',
            'show_nationality' => 'Show Nationality',
            'show_drivinglicence' => 'Show Driving Licence Info',
            'show_birthday' => 'Show Birthday',
            'show_birthplace' => 'Show Birthplace',
            'show_phone' => 'Show Phone Number',
            'show_mobile' => 'Show Mobile Number',
            'show_address' => 'Show Address',
            'show_familystatus' => 'Show Familystatus',
            'show_denomination' => 'Show Denomination',
            'show_children' => 'Show Children Information',
            'show_parents' => 'Show Parents Information',
        ],
        'tabs' => [
            'status' => 'Status',
            'sections' => 'Sections',
            'profile' => 'Profile',
            'attachments' => 'Attachments',
            'themes' => 'Themes',
        ],
        'comments' => [
            'sections' => 'Default values, if sections should be shown in a new Application',
            'profile' => 'Default values, if profile fields should be shown in a new Application',
        ],
        'validation' => [
            'theme_not_found' => 'The theme is not found',
        ],
    ],
    'actions' => [
        'model' => [
            'name' => 'Action',
            'title' => 'Actions',
        ],
        'buttons' => [
            'choose_theme' => 'Choose theme',
            'preview' => 'Application Preview',
            'send_test_mail' => 'Send Test Mail',
            'send_application_mail' => 'Send Application Mail',
        ],
        'labels' => [
            'devolution' => 'Devolution',
            'date' => 'Date',
            'mailsend' => 'Mail send to Contact Person',
            'login' => 'Successful Login by Contact Person',
        ],
    ],
];

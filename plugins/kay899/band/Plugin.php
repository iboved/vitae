<?php namespace Kay899\Band;

use Backend;
use System\Classes\PluginBase;
use System\Controllers\Settings;
use Backend\Models\User as BackendUserModel;
use Kay899\Band\Models\Profile as ProfileModel;
use October\Rain\Database\Relations\Relation;


/**
 * Band Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Band',
            'description' => 'Rockability Plugin',
            'author'      => 'kay899',
            'icon'        => 'icon-music'
        ];
    }

    public function registerComponents()
    {
        return [
            'Kay899\Band\Components\ProfileList' => 'profileList',
            'Kay899\Band\Components\ProfileDetail' => 'profileDetail',
            'Kay899\Band\Components\Setlist' => 'setlist',
            'Kay899\Band\Components\Probenzettel' => 'probenzettel',
            'Kay899\Band\Components\AppointmentList' => 'appointmentlist',
            'Kay899\Band\Components\GigSetlist' => 'gigSetlist'
        ];
    }

    public function registerPageSnippets()
    {
        return [
            'Kay899\Band\Components\ProfileList' => 'profileList',
            'Kay899\Band\Components\Setlist' => 'setlist'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        BackendUserModel::extend(function($model)
        {
            // Add relation for Profile
            $model->hasOne['profile'] = ['Kay899\Band\Models\Profile'];

            // Ensure that every Backend User within Group "bandmember" gets a profile
            $model->bindEvent('model.afterSave', function() use ($model)
            {
                if ($model->groups()->whereName('bandmember')->exists())
                {
                    if($model->profile)
                    {
                        return;
                    }
                    else
                    {
                        $profile = new ProfileModel;
                        $profile->user = $model;
                        $profile->save();
                        $model->profile = $profile;
                        return;
                    }
                }
            });
        });

        Relation::morphMap([
            'profile' => 'Kay899\Band\Models\Profile',
        ]);
    }

   /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'band' => [
                'label'       => 'kay899.band::lang.plugin.name',
                'url'         => Backend::url('kay899/band/songs'),
                'icon'        => 'icon-music',
                'order'       => 20,

                'sideMenu' => [
                    'songs' => [
                        'label'       => 'kay899.band::lang.songs.model.title',
                        'url'         => Backend::url('kay899/band/songs'),
                        'icon'        => 'icon-music',
                        'order'       => 100,
                    ],
                    'profiles' => [
                        'label'       => 'kay899.band::lang.profiles.model.title',
                        'url'         => Backend::url('kay899/band/profiles'),
                        'icon'        => 'icon-male',
                        'order'       => 101,
                    ],
                    'galleries' => [
                        'label'       => 'kay899.band::lang.galleries.model.title',
                        'url'         => Backend::url('kay899/band/galleries'),
                        'icon'        => 'icon-image',
                        'order'       => 102,
                    ],
                    'gigs' => [
                        'label'       => 'kay899.band::lang.gigs.model.title',
                        'url'         => Backend::url('kay899/band/gigs'),
                        'icon'        => 'icon-microphone',
                        'order'       => 103,
                    ],
                    'appointments' => [
                        'label'       => 'kay899.band::lang.appointments.model.title',
                        'url'         => Backend::url('kay899/band/appointments'),
                        'icon'        => 'icon-calendar',
                        'order'       => 104,
                    ],
                ],
            ],
        ];
    }

}

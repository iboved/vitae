<?php namespace Stargate\Project\Console;

use DB;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Backend\Models\UserRole;

class ImportRoles extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'project:importroles';

    /**
     * @var string The console command description.
     */
    protected $description = 'Import Script for Role from SG-P to October version';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {

            #$roles = DB::connection('mysql_old')->table('roles')->where('id', 1)->get();
            $roles = DB::connection('mysql_old')->table('roles')->get();

            DB::table('backend_user_roles')->truncate();

            foreach ($roles as $item) {
                $new = new UserRole();
                $new->id = $item->id;
                $new->name = $item->title;
                $new->code = str_slug($item->title);
                $new->description = $item->description;
                $new->is_system = $item->level > 9 ? 1 : 0;
                $new->created_at = $item->created_at;
                $new->updated_at = $item->updated_at;
                $new->save();
            }
        });

        $this->output->writeln('Roles imported');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}

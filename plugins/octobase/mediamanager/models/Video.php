<?php namespace Octobase\Mediamanager\Models;

use Model;

/**
 * Video Model
 */
class Video extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \October\Rain\Database\Traits\Sortable;

    use \Octobase\Backend\Traits\Base;

    const SORT_ORDER = 'sort_order';

    protected $dates = ['deleted_at'];

    /**
     * Validation rules
     */
    public $rules = [
        'title' => 'required',
        'videolink' => 'required',
    ];

    /**
     * Attribute names
     */
    public $attributeNames = [
        'title' => 'Titel',
        'videolink' => 'Video-Link',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'octobase_mediamanager_videos';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
}

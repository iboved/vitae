<?php namespace Vitae\Dashboard\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Backend\Widgets\ReportContainer;

/**
 * Dashboard Back-end Controller
 */
class Dashboard extends Controller
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Vitae.Dashboard', 'dashboard', 'dashboard');

        $this->addCss('/modules/backend/assets/css/dashboard/dashboard.css', 'core');
    }

    public function index()
    {
        $this->initReportContainer();

        $this->pageTitle = 'Vitae Dashboard';
    }

    public function index_onInitReportContainer()
    {
        $this->initReportContainer();

        return ['#dashReportContainer' => $this->widget->reportContainer->render()];
    }

    /**
     * Prepare the report widget used by the dashboard
     * @param Model $model
     * @return void
     */
    protected function initReportContainer()
    {
        new ReportContainer($this, 'config_dashboard.yaml');
    }

}

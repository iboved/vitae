<?php namespace Stargate\Content;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * Content Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'SG-P Content',
            'description' => 'Content SG-P Plugin',
            'author'      => 'Andreas Hammen',
            'icon'        => 'icon-tags'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'stargate.content.manage_content' => [
                'tab' => 'stargate.project::lang.permissions.tab',
                'label' => 'stargate.project::lang.permissions.manage_content'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'content' => [
                'label'       => 'Content',
                'url'         => Backend::url('stargate/content/categories'),
                'icon'        => 'icon-tags',
                'permissions' => ['stargate.content.manage_content'],
                'order'       => 440,

                'sideMenu' => [
                    'categories' => [
                        'label'       => 'octobase.categories::lang.categories.model.title',
                        'url'         => Backend::url('stargate/content/categories'),
                        'icon'        => 'icon-tags',
                        'order'       => 100,
                    ],
                    'banners' => [
                        'label'       => 'octobase.advertising::lang.banners.model.title',
                        'url'         => Backend::url('stargate/content/banners'),
                        'icon'        => 'icon-server',
                        'order'       => 105,
                    ],
                    'teasers' => [
                        'label'       => 'Teaser',
                        'url'         => Backend::url('stargate/content/teasers'),
                        'icon'        => 'icon-map-signs',
                        'order'       => 110,
                    ],
                ],
            ],
        ];
    }
}

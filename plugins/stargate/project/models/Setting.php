<?php namespace Stargate\Project\Models;

use Model;
use Octobase\Categories\Models\Category;
use October\Rain\Exception\ApplicationException;

/**
 * Setting Model
 */
class Setting extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'stargate-project-settings';

    public $settingsFields = 'fields.yaml';

    public function afterSave()
    {
        $filePath = storage_path('app/uploads/public/settings.json');

        if (!$settingsFile = fopen($filePath, 'w')) {
            throw new ApplicationException("Cannot open file ($filePath)");
        }

        if (fwrite($settingsFile, json_encode($this->value)) === false) {
            throw new ApplicationException("Cannot write to file ($filePath)");
        }

        fclose($settingsFile);
    }

	public function categoryRoots($fieldName, $value, $formData)
	{
		return Category::getAllRoot()->pluck('title', 'id');
	}

	public function categoryAll($fieldName, $value, $formData)
	{
		return Category::all()->pluck('title', 'id');
	}
}

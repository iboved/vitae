<?php namespace Octobase\MediaManager\Models;

use Model;

/**
 * Folder Model
 */
class Folder extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'octobase_mediamanager_folders';

    /**
     * Validation rules
     */
    public $rules = [
        'title' => 'required',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public $hasMany = [
        'images' => 'Octobase\MediaManager\Models\Image',
        'images_count' => ['Octobase\MediaManager\Models\Image', 'count' => true]
    ];

    public function getIsGalleryLabelAttribute()
    {
        $value = array_get($this->attributes, 'is_gallery');

        $is_gallery = $value ? trans('backend::lang.list.column_switch_true') : '';

        return $is_gallery;
    }
}

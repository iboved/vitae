<?php namespace Stargate\Project\Console;

use DB;
use Hash;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Octobase\Categories\Models\Category;

class ImportCategories extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'project:importcategories';

    /**
     * @var string The console command description.
     */
    protected $description = 'Import Script for Categories from SG-P to October version';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {

            #$categories = DB::connection('mysql_old')->table('categories')->where('id', 1)->get();
            $categories = DB::connection('mysql_old')->table('categories')->get();

            // Nested Sets Trait must be inactive while Import !!!

            DB::table('octobase_categories_categories')->truncate();

            foreach ($categories as $item) {
                $new = new Category();
                $new->id = $item->id;
                $new->title = $item->title;
                $new->parent_id = $item->parent_id;
                $new->lft = $item->lft;
                $new->rgt = $item->rgt;
                $new->depth = $item->depth;
                $new->is_active = $item->active;
                $new->created_by = $item->created_by;
                $new->updated_by = $item->updated_by;
                $new->deleted_by = $item->deleted_by;
                $new->created_at = $item->created_at;
                $new->updated_at = $item->updated_at;
                $new->deleted_at = $item->deleted_at;
                $new->save();
            }
        });

        $this->output->writeln('Categories imported');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}

<?php 

return [
    'clipboard' => [
        'address' => 'Adresse',
        'application' => 'BEWERBUNG',
        'education' => 'AUSBILDUNG',
        'experience' => 'BERUFSERFAHRUNG',
        'skills' => 'FAEHIGKEITEN',
        'other' => 'SONSTIGES',
        'attachments' => 'ANHAENGE',
    ],
];



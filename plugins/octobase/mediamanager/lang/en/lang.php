<?php

return [
    'plugin' => [
        'name' => 'MediaManager',
        'description' => 'Octobase MediaManager Extension',
    ],
    'images' => [
        'model' => [
            'name' => 'Image',
            'title' => 'Images',
            'create' => 'Create Image',
            'update' => 'Update Image',
            'reorder' => 'Reorder Images',
        ],
        'labels' => [
            'title' => 'Title',
            'file_name' => 'Filename',
            'file_url' => 'Image Preview',
            'folder' => 'Folder',
            'copyright' => 'Copyright',
        ],
        'validation' => [
            'title_cant_be_empty' => 'The title cannot be empty',
        ],
    ],
    'folders' => [
        'model' => [
            'name' => 'Folder',
            'title' => 'Folders',
            'create' => 'Create Folder',
            'update' => 'Update Folder',
            'reorder' => 'Reorder Folders',
        ],
        'labels' => [
            'title' => 'Title',
            'folder_name' => 'Folder',
            'images_count' => 'Image Count',
            'is_gallery' => 'Is Gallery',
        ],
        'validation' => [
            'title_cant_be_empty' => 'The title cannot be empty',
        ],
    ],
    'videos' => [
        'model' => [
            'name' => 'Youtube-Video',
            'title' => 'Youtube-Videos',
            'create' => 'Create Youtube-Video',
            'update' => 'Update Youtube-Video',
            'reorder' => 'Reorder Youtube-Videos',
        ],
        'labels' => [
            'title' => 'Title',
            'text' => 'Text',
            'videolink' => 'Video-Link',
            'sort_order' => 'Sorting',
            'active' => 'Active',
        ],
        'validation' => [
            'title_cant_be_empty' => 'The title cannot be empty',
            'videolink_cant_be_empty' => 'The video-link cannot be empty',
        ],
    ],
    'buttons' => [
        'without_title' => 'Images without title',
    ],
    'filters' => [
        'without_title' => 'Images without title',
        'folder' => 'Folder',
    ],
    'settings' => [
        'tabs' => [
            'mediamanager' => 'MediaManager',
        ],
        'labels' => [
            'is_active' => 'MediaManager Extension active',
            'show_is_gallery' => 'Show Is Gallery',
            'show_copyright' => 'Show Copyright',
            'show_youtube_module' => 'Show Youtube Module',
            'show_youtube_is_active' => 'Show Active for Youtube Module',
        ],
    ],
];
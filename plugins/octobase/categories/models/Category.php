<?php namespace Octobase\Categories\Models;

use Model;
use Octobase\Backend\Traits\Base;
use October\Rain\Database\Traits\NestedTree;
use October\Rain\Database\Traits\SoftDelete;
use October\Rain\Database\Traits\Validation;
use October\Rain\Exception\ValidationException;
use Octobase\Backend\Models\Setting;

/**
 * Category Model
 */
class Category extends Model
{
    use Validation, SoftDelete, NestedTree, Base;

    const NEST_LEFT = 'lft';
    const NEST_RIGHT = 'rgt';
    const NEST_DEPTH = 'depth';

    public $table = 'octobase_categories_categories';

    public $rules = [
        'title' => 'required',
    ];

    public $attributeNames = [
        'title' => 'octobase.categories::lang.categories.labels.title',
    ];

    protected $dates = ['deleted_at'];

    public function beforeValidate()
    {
        $maxDepth = Setting::get('category_max_depth', 0);

        $targetNode = $this->parent;

        $totalDepth = $this->getMaxNodesDepth();

        if ($targetNode) {
            $totalDepth += $targetNode->getDepth();
        } else {
            $totalDepth--;
        }

        if ($totalDepth > $maxDepth) {
            throw new ValidationException(['message' => trans('octobase.categories::lang.categories.errors.max_depth')]);
        }
    }

    public function getMaxNodesDepth()
    {
        if (!$this->exists) {
            return 1;
        }

        $maxDepthNode = $this->newQuery()
            ->allChildren(true)
            ->orderBy('depth', 'desc')
            ->first();

        $maxNodesDepth = $maxDepthNode->newQuery()
            ->parents(true)
            ->where('lft', '>=', $this->getLeft())
            ->where('lft', '<', $this->getRight())
            ->count();

        return $maxNodesDepth;
    }

    public function getRootsOptions()
    {
        $roots = $this->getAllRoot()->pluck('title', 'id');

        return $roots;
    }
}

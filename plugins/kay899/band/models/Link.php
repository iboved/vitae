<?php namespace Kay899\Band\Models;

use Model;

/**
 * Link Model
 */
class Link extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \Kay899\Band\Traits\Base;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kay899_band_links';

    /*
     * Validation
     */
    public $rules = [
        'title' => 'required',
        'link' => 'required|url',
    ];
    
}

<?php namespace Kay899\Band\Components;

use Cms\Classes\ComponentBase;
use Kay899\Band\Models\Gig;

class GigSetlist extends ComponentBase
{
    /**
     * Array of gigSetlist
     * @var array
     */
    public $gigSetlist;

    public function componentDetails()
    {
        return [
            'name'        => 'Gig Setlist',
            'description' => 'Setlist per Gig'
        ];
    }

    public function onRun()
    {
        $id = $this->getRouter()->getParameters()['id'];

        $gig = Gig::where('id', $id)->with(['songs' => function ($query) {
            $query->orderBy('gig_sort_order');
        }])->first();
        $this->gig = $this->page['gig'] = $gig;

        if ($gig) {
            $songs = $gig->songs->sortBy('pivot.set')->groupBy('pivot.set');
            $this->songs = $this->page['songs'] = $songs;
        }
    }
}

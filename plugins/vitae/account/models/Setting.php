<?php namespace Vitae\Account\Models;

use Lang;
use Config;
use Model;
use Backend\Models\UserRole;
use Backend\Models\UserGroup;
use System\Models\MailTemplate;
use Vitae\Vita\Classes\Helper;

/**
 * Setting Model
 */
class Setting extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'vitae-account-settings';

    public $settingsFields = 'fields.yaml';

    public function getRoleOptions()
    {
        $result = [];

        foreach (UserRole::all() as $role)
        {
            $result[$role->id] = [$role->name];
        }

        return $result;
    }

    public function getGroupOptions()
    {
        $result = [];

        foreach (UserGroup::all() as $group)
        {
            $result[$group->id] = [$group->name];
        }

        return $result;
    }

    public function getWelcomeTemplateOptions()
    {
        $codes = array_keys(MailTemplate::listAllTemplates());

        $result = [''=>'- '.Lang::get('rainlab.user::lang.settings.no_mail_template').' -'];

        $result += array_combine($codes, $codes);

        return $result;
    }

    public function getLocaleOptions()
    {
        return Helper::locales();
    }

    public function getTimezoneOptions()
    {
        return Helper::timezones();
    }
}
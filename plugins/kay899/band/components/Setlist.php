<?php namespace Kay899\Band\Components;

use Cms\Classes\ComponentBase;
use Kay899\Band\Models\Song;

class Setlist extends ComponentBase
{
    /**
     * Array of setlist
     * @var array
     */
    public $setlist;

    public function componentDetails()
    {
        return [
            'name'        => 'Setlist',
            'description' => 'Band Setlist'
        ];
    }

    // This array becomes available on the page as {{ component.songs }}
    public function songs()
    {
        return Song::where('status', 3)->where('is_public', 1)->get();
    }
}

<?php namespace Vitae\Vita\Controllers;

use Backend\Classes\Controller;
use Backend\Models\User;
use BackendMenu;
use BackendAuth;
use Session;
use Input;
/**
 * Attachments Back-end Controller
 */
class Attachments extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController',
        'Vitae\Account\Behaviors\ModalController',
        'Vitae\Account\Behaviors\MultiTenantController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();

        $this->pageTitle = 'vitae.vita::lang.attachments.model.title';

        BackendMenu::setContext('Vitae.Vita', 'vita', 'attachments');

        if (post('create_mode') || post('file_id')) {
            $this->asExtension('FormController')->create();
        }

        if (post('update_mode')) {
            $this->asExtension('FormController')->update(post('record_id'));
        }
    }
}
<?php namespace Octobase\MediaManager\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Octobase\Backend\Models\Setting;

/**
 * Folders Back-end Controller
 */
class Folders extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('October.Backend', 'media', 'folders');
    }

    // Adding Form to Modal window for udpate
    public function onUpdateForm()
    {
        $this->asExtension('FormController')->update(post('record_id'));

        $this->vars['recordId'] = post('record_id');
        $this->vars['extension'] = !empty($extension) ? '.' . $extension : '';

        return $this->makePartial('form_update');
    }

    // Saving data for update from Modal window
    public function onUpdate()
    {
        $this->asExtension('FormController')->update_onSave(post('record_id'));

        return $this->listRefresh();
    }

    public function listExtendColumns($list)
    {
        if (!Setting::get('mediamanager_show_is_gallery')) {
            $list->removeColumn('is_gallery_label');
        }
    }

    public function formExtendFields($form)
    {
        if (!Setting::get('mediamanager_show_is_gallery')) {
            $form->removeField('is_gallery');
        }
    }
}

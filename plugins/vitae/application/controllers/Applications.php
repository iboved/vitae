<?php namespace Vitae\Application\Controllers;

use Event;
use Flash;
use Log;
use Session;
use BackendMenu;
use BackendAuth;
use Backend\Classes\Controller;
use Vitae\Application\Models\Application;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

/**
 * Applications Back-end Controller
 */
class Applications extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        $this->pageTitle = 'vitae.application::lang.applications.model.title';

        BackendMenu::setContext('Vitae.Application', 'application', 'applications');
    }

    public function onSendTestMail()
    {
        $application = Application::find(post('model_id'));

        $data = [
            'client_name' => $application->user->full_name,
            'application_contact' => $application->contact,
            'application_url' => env('APP_MYVITA') . $application->applicationurl,
            'auth_token' => $application->authtoken,
        ];

        try {
            Mail::send('vitae.application::mail.application', $data, function ($message) use ($application){
                $message->to($application->user->email, $application->user->full_name);
            });

            Flash::success(trans('vitae.application::lang.applications.messages.mail_success'));
        } catch (\Exception $exception) {
            Log::error(sprintf('Failed to send mail. Error details: %s', $exception));

            Flash::error(trans('vitae.application::lang.applications.messages.mail_error'));
        }
    }

    public function onSendApplicationMail() // TODO: only active, if Plan is active
    {
        $application = Application::find(post('model_id'));

        $data = [
            'client_name' => $application->user->full_name,
            'application_contact' => $application->contact,
            'application_url' => env('APP_MYVITA') . $application->applicationurl,
            'auth_token' => $application->authtoken,
        ];

        try {
            Mail::send('vitae.application::mail.application', $data, function ($message) use ($application){
                $message->to($application->email, $application->user->full_name);
            });

            Event::fire('application.mailsend', [$application]);

            Flash::success(trans('vitae.application::lang.applications.messages.mail_success'));
        } catch (\Exception $exception) {
            Log::error(sprintf('Failed to send mail. Error details: %s', $exception));

            Flash::error(trans('vitae.application::lang.applications.messages.mail_error'));
        }
    }

    public function listExtendColumns($list)
    {
        if (!BackendAuth::getUser()->is_superuser)
        {
            $list->removeColumn('user_id');
        }
    }

    public function preview($recordId)
    {
        $application = Application::find($recordId);

        if (!$application) {
            return Response::make(View::make('cms::404'), 404);
        }

        Session::put('applications.' . $application->id, $application->authtoken);

        return Redirect::to('/myvita/' . $application->applicationurl);
    }
    
}

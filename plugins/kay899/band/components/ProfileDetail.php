<?php namespace Kay899\Band\Components;

use Cms\Classes\ComponentBase;
use Backend\Models\User as BackendUserModel;

class ProfileDetail extends ComponentBase
{
    /**
     * @var profile object
     */
    public $profile;

    public function componentDetails()
    {
        return [
            'name'        => 'Profil-Detail',
            'description' => 'Band Profil Detail'
        ];
    }

    public function onRun()
    {
        $slug = $this->getRouter()->getParameters()['slug'];

        $profile = BackendUserModel::where('first_name', $slug)->first()->profile;

        if (!$profile)
        {
            return \Response::make($this->controller->run('404'), 404);
        }

        $this->profile = $this->page['profile'] = $profile;

        $this->page->title = $profile->user_id_label;
        $this->page->tagline = $profile->position;
    }
}

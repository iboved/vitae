<?php

return [
    'plugin' => [
        'name' => 'Octobase Backend',
        'description' => 'Backend Settings and usefull AddOns',
        'backend_settings' => 'Octobase', 
    ],
    'settings' => [
        'model' => [
            'name' => 'Setting',
            'title' => 'Settings',
            'description' => 'Manage general Backend Settings',
        ],
        'tabs' => [
            'backend' => 'Backend',
        ],
        'labels' => [
            'settings_one_column' => 'Show Backend Settings in one column',
        ],
    ],
    'lists' => [
        'labels' => [
            'yes' => 'Yes',
            'no' => 'No',
            'created_at' => 'Created',
            'updated_at' => 'Updated',
            'sort_order' => 'Sortorder',
        ],
    ],
];
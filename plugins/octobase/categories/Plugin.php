<?php namespace Octobase\Categories;

use Backend;
use Event;
use System\Classes\PluginBase;
use Octobase\Backend\Models\Setting;
use Octobase\Categories\Models\Category;

/**
 * Categories Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'octobase.categories::lang.plugin.name',
            'description' => 'octobase.categories::lang.plugin.description',
            'author'      => 'Octobase',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Event::listen('backend.form.extendFields', function ($widget)
        {
            if (!$widget->model instanceof Setting) {
                return;
            }

            $widget->addTabFields([
                'category' => [
                    'label' => 'octobase.backend::lang.settings.model.title',
                    'type' => 'section',
                    'tab' => 'octobase.categories::lang.settings.tabs.categories',
                ],
                'category_show_navigation' => [
                    'label' => 'octobase.categories::lang.settings.labels.show_navigation',
                    'type' => 'switch',
                    'span' => 'left',
                    'tab' => 'octobase.categories::lang.settings.tabs.categories',
                ],
                'category_max_depth' => [
                    'label' => 'octobase.categories::lang.settings.labels.max_depth',
                    'type' => 'balloon-selector',
                    'options' => [
                        1 => 1,
                        2 => 2,
                        3 => 3,
                        4 => 4,
                        5 => 5,
                    ],
                    'span' => 'right',
                    'tab' => 'octobase.categories::lang.settings.tabs.categories',
                ],
                'category_show_active' => [
                    'label' => 'octobase.categories::lang.settings.labels.show_active',
                    'type' => 'switch',
                    'span' => 'left',
                    'tab' => 'octobase.categories::lang.settings.tabs.categories',
                ],
                'category_tree_expanded' => [
                    'label' => 'octobase.categories::lang.settings.labels.tree_expanded',
                    'type' => 'switch',
                    'span' => 'left',
                    'tab' => 'octobase.categories::lang.settings.tabs.categories',
                ],
            ]);
        });
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'octobase.categories.manage_settings' => [
                'tab' => 'octobase.backend::lang.plugin.backend_settings',
                'label' => 'octobase.categories::lang.categories.model.title'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // Settings show navigation, otherwise it has to be implented within an other Plugin
        if (Setting::get('category_show_navigation'))
        {
            return [
                'categories' => [
                    'label'       => 'octobase.categories::lang.plugin.name',
                    'url'         => Backend::url('octobase/categories/categories'),
                    'icon'        => 'icon-tags',
                    'permissions' => ['octobase.categories.*'],
                    'order'       => 500,

                    'sideMenu' => [
                        'categories' => [
                            'label'       => 'octobase.categories::lang.categories.model.title',
                            'url'         => Backend::url('octobase/categories/categories'),
                            'icon'        => 'icon-tags',
                            'order'       => 100,
                            'permissions' => ['octobase.categories.*'],
                        ],
                    ],
                ],
            ];
        }

        return [];
    }
}

<?php namespace Octobase\MediaManager\Models;

use Model;
use System\Classes\MediaLibrary;
use BackendAuth;
use Str;

/**
 * Image Model
 */
class Image extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'octobase_mediamanager_images';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public $belongsTo = [
        'folder' => 'Octobase\MediaManager\Models\Folder',
    ];

    public function beforeSave()
    {
        if (post('Image')) {
            if (Str::lower($this->getOriginal('file_name')) !== Str::lower($this->file_name)) {
                $oldPath = str_replace_first('storage/app/media','', $this->file_url);
                $newPath = dirname($oldPath).'/'.$this->file_name;

                MediaLibrary::instance()->moveFile($oldPath, $newPath);
                MediaLibrary::instance()->resetCache();

                $this->file_url = dirname($this->file_url).'/'.$this->file_name;
            }

            $this->updated_by = BackendAuth::getUser()->id;
        }
    }
}
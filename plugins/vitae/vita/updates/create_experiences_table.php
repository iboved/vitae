<?php namespace Vitae\Vita\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateExperiencesTable extends Migration
{
    public function up()
    {
        Schema::create('vitae_vita_experiences', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('corporation');
            $table->string('position')->nullable();
            $table->tinyInteger('mode');
            $table->tinyInteger('month_from')->nullable();
            $table->smallInteger('year_from')->nullable();
            $table->tinyInteger('month_till')->nullable();
            $table->smallInteger('year_till')->nullable();
            $table->boolean('till_today')->default(0);
            $table->integer('sort_order')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('vitae_vita_experiences');
    }
}

<?php namespace Octobase\Advertising;

use Backend;
use Event;
use System\Classes\PluginBase;
use Octobase\Backend\Models\Setting;
use Octobase\Advertising\Models\Banner;

/**
 * Advertising Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'octobase.advertising::lang.plugin.name',
            'description' => 'octobase.advertising::lang.plugin.description',
            'author'      => 'Octobase',
            'icon'        => 'icon-server'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Event::listen('backend.form.extendFields', function ($widget)
        {
            if (!$widget->model instanceof Setting) {
                return;
            }

            $widget->addTabFields([
                'advertising' => [
                    'label' => 'octobase.backend::lang.settings.model.title',
                    'type' => 'section',
                    'tab' => 'octobase.advertising::lang.settings.tabs.advertising',
                ],
                'advertising_show_navigation' => [
                    'label' => 'octobase.advertising::lang.settings.labels.show_navigation',
                    'type' => 'switch',
                    'span' => 'left',
                    'tab' => 'octobase.advertising::lang.settings.tabs.advertising',
                ],
                'advertising_positions' => [
                    'label' => 'octobase.advertising::lang.settings.labels.positions',
                    'type' => 'text',
                    'commentAbove' => 'octobase.advertising::lang.settings.hints.positions',
                    'span' => 'right',
                    'tab' => 'octobase.advertising::lang.settings.tabs.advertising',
                ],
                'advertising_show_active' => [
                    'label' => 'octobase.advertising::lang.settings.labels.show_active',
                    'type' => 'switch',
                    'span' => 'left',
                    'tab' => 'octobase.advertising::lang.settings.tabs.advertising',
                ],
            ]);
        });
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'octobase.advertising.manage_settings' => [
                'tab' => 'octobase.backend::lang.plugin.backend_settings',
                'label' => 'octobase.advertising::lang.banners.model.title'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // Settings show navigation, otherwise it has to be implented within an other Plugin
        if (Setting::get('advertising_show_navigation'))
        {
            return [
                'advertising' => [
                    'label'       => 'octobase.advertising::lang.plugin.name',
                    'url'         => Backend::url('octobase/advertising/banners'),
                    'icon'        => 'icon-server',
                    'permissions' => ['octobase.advertising.*'],
                    'order'       => 505,

                    'sideMenu' => [
                        'advertising' => [
                            'label'       => 'octobase.advertising::lang.banners.model.title',
                            'url'         => Backend::url('octobase/advertising/banners'),
                            'icon'        => 'icon-server',
                            'order'       => 100,
                            'permissions' => ['octobase.advertising.*'],
                        ],
                    ],
                ],
            ];
        }

        return [];
    }
}

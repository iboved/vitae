<?php namespace Stargate\Gatepedia;

use Backend;
use Illuminate\Support\Facades\Event;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'SG-P Gatepedia',
            'description' => 'Gatepedia SG-P Plugin',
            'author'      => 'Andreas Hammen',
            'icon'        => 'icon-desktop'
        ];
    }

    public function boot()
    {
        Event::listen('backend.filter.extendScopes', function ($widget) {
            $scopes = $widget->getScopes();

            $switchScope = array_first($scopes, function ($scope, $key) {
                return $scope->type == 'switch';
            });

            if ($switchScope) {
                $widget->getController()->addJs('/plugins/stargate/gatepedia/assets/js/checkbox.js');
                $widget->addViewPath(plugins_path('stargate/gatepedia/widgets/filter/partials'));
            }
        });
    }

    public function registerComponents()
    {
    	return [
    		'Stargate\Gatepedia\Components\SeriesList' => 'seriesList'
    	];
    }

    public function registerPageSnippets()
    {
        return [
            'Stargate\Gatepedia\Components\SeriesList' => 'seriesList'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'stargate.gatepedia.manage_gatepedia' => [
                'tab' => 'stargate.project::lang.permissions.tab',
                'label' => 'stargate.project::lang.permissions.manage_gatepedia'
            ],
        ];
    }

   /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'gatepedia' => [
                'label'       => 'Gatepedia',
                'url'         => Backend::url('stargate/gatepedia/series'),
                'icon'        => 'icon-desktop',
                'permissions' => ['stargate.gatepedia.manage_gatepedia'],
                'order'       => 450,

                'sideMenu' => [
                    'series' => [
                        'label'       => 'Serien',
                        'url'         => Backend::url('stargate/gatepedia/series'),
                        'icon'        => 'icon-desktop',
                        'order'       => 100,
                    ],
                    'films' => [
                        'label'       => 'Filme',
                        'url'         => Backend::url('stargate/gatepedia/films'),
                        'icon'        => 'icon-film',
                        'order'       => 101,
                    ],
                ],
            ],
        ];
    }
}

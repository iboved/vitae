function initializeSorting() {
    var $tbody = $('.drag-handle').parents('table.data tbody');

    if ($tbody.length) {
        Sortable.create($tbody[0], {
            handle: '.drag-handle',
            animation: 150,
            onEnd: function (evt) {
                var $inputs = $(evt.srcElement).find('td>div.drag-handle>input');
                var $form = $('<form style="display: none;">');
                $form.append($inputs.clone())
                    .request('onReorder', {
                        loading: $.oc.stripeLoadIndicator,
                        data: {model_id: $("input[name='model_id']").val()},
                        complete: function () {
                            $form.remove();
                        }
                    });
            }
        });
    }
}

$(function () {
    initializeSorting();

    $(document).ajaxComplete(function() {
        initializeSorting();
    });
});

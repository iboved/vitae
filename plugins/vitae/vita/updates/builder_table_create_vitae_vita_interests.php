<?php namespace Vitae\Vita\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVitaeVitaInterests extends Migration
{
    public function up()
    {
        Schema::create('vitae_vita_interests', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title', 255);
            $table->integer('sort_order');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vitae_vita_interests');
    }
}

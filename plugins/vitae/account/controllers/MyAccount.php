<?php namespace Vitae\Account\Controllers;

use BackendAuth;
use BackendMenu;
use Backend\Classes\Controller;
use System\Classes\SettingsManager;

/**
 * My Account Back-end Controller
 */
class MyAccount extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
    ];

    public $formConfig = 'config_form.yaml';

    /**
     * @var string HTML body tag class
     */
    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        $this->pageTitle = 'vitae.account::lang.myaccount.model.title';

        BackendMenu::setContext('Vitae.Account', 'account', 'myaccount');
    }

    public function index()
    {
        SettingsManager::setContext('Vitae.Account', 'myaccount');

        return $this->update($this->user->id, 'myaccount');
    }

    /**
     * Proxy update onSave event
     */
    public function index_onSave()
    {
        $result = $this->asExtension('FormController')->update_onSave($this->user->id, 'profile');

        /*
         * If the password or email has been updated, reauthenticate the user
         */
        $emailChanged = $this->user->email != post('User[email]');

        $passwordChanged = strlen(post('User[password]'));

        if ($emailChanged || $passwordChanged)
        {
            BackendAuth::login($this->user->reload(), true);
        }

        return $result;
    }
}

<?php namespace Octobase\Categories\Controllers;

use Backend;
use BackendMenu;
use Backend\Classes\Controller;
use Octobase\Backend\Models\Setting;
use Octobase\Categories\Models\Category;
use October\Rain\Support\Facades\Flash;
use Illuminate\Support\Facades\Session;
use Str;

/**
 * Categories Back-end Controller
 */
class Categories extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ReorderController',
        'Octobase\Backend\Behaviors\ModalController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = '$/octobase/categories/controllers/categories/config_list_close.yaml';
    public $reorderConfig = '$/octobase/categories/controllers/categories/config_reorder.yaml';

    public function __construct()
    {
        if (Setting::get('category_tree_expanded')) {
            $this->listConfig = '$/octobase/categories/controllers/categories/config_list_open.yaml';
        }

        parent::__construct();

        BackendMenu::setContext('Octobase.Categories', 'categories', 'categories');
    }

    public function reorder()
    {
        $config = $this->makeConfig($this->formConfig, ['defaultRedirect']);

        $this->vars['defaultRedirect'] = Backend::url($config->defaultRedirect);

        $this->asExtension('ReorderController')->reorder();
    }

    public function onReorder()
    {
        $maxDepth = Setting::get('category_max_depth', 0);

        $model = $this->reorderGetModel();

        $sourceNode = $model->find(post('sourceNode'));
        $targetNode = $model->find(post('targetNode'));

        $totalDepth = $sourceNode->getMaxNodesDepth();

        if ($targetNode) {
            $totalDepth += $targetNode->getDepth();
        }

        if (post('position') != 'child') {
            $totalDepth--;
        }

        if ($totalDepth > $maxDepth) {
            Flash::error(trans('octobase.categories::lang.categories.errors.max_depth'));

            $this->reorder();

            return [
                '#reorder-list' => $this->reorderRender()
            ];
        }

        $this->asExtension('ReorderController')->onReorder();
    }

    public function onToggleTreeNodes()
    {
        $categoryIds = Category::lists('id');

        $nodeStatuses = [];

        foreach ($categoryIds as $categoryId) {
            $nodeStatuses['tree_node_status_' . $categoryId] = post('expand', 0);
        }

        if (!empty($nodeStatuses)) {
            $sessionId = $this->makeSessionIdCustom();

            Session::put($sessionId, base64_encode(serialize($nodeStatuses)));
        }

        return $this->listRefresh();
    }

    public function listGetConfig($definition = null)
    {
        $config = $this->asExtension('ListController')->listGetConfig($definition);

        if (Setting::get('category_show_active')) {
            $config->list = '$/octobase/categories/models/category/columns_active.yaml';
        }

        return $config;
    }

    /**
     * Returns a unique session identifier for this widget and controller action.
     * @return string
     */
    protected function makeSessionIdCustom()
    {
        $controller = property_exists($this, 'controller') && $this->controller
            ? $this->controller
            : $this;

        // Removes Class name and "Controllers" directory
        $rootNamespace = Str::getClassId(Str::getClassNamespace(Str::getClassNamespace($controller)));

        // The controller action is intentionally omitted, session should be shared for all actions
        return 'widget.' . $rootNamespace . '-' . class_basename($controller) . '-Lists';
    }
}
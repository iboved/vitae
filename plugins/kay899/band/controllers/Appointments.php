<?php namespace Kay899\Band\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Appointments Back-end Controller
 */
class Appointments extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kay899.Band', 'band', 'appointments');
    }
}

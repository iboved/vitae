<?php namespace Vitae\Vita\Controllers;

use BackendAuth;
use BackendMenu;
use Backend\Classes\Controller;
use System\Classes\SettingsManager;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Session;

/**
 * Profile Back-end Controller
 */
class Profile extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    /**
     * @var string HTML body tag class
     */
    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        $this->pageTitle = 'vitae.vita::lang.profile.model.title';  

        BackendMenu::setContext('Vitae.Vita', 'vita', 'profile');
    }

    public function index()
    {
        if (BackendAuth::getUser()->is_superuser) {
            return $this->asExtension('ListController')->index();
        }

        SettingsManager::setContext('October.Backend', 'profile');

        return $this->update($this->user->id, 'profile');
    }

    public function update($recordId, $context = null)
    {
        $user = BackendAuth::getUser();

        if (!$user->is_superuser && $user->id != $recordId) {
            return Response::make(View::make('cms::404'), 404);
        }

        if ($user->is_superuser) {
            Session::put('user_id', $recordId);
        }

        return $this->asExtension('FormController')->update($recordId, $context);
    }

    /**
     * Proxy update onSave event
     */
    public function index_onSave()
    {
        $result = $this->asExtension('FormController')->update_onSave($this->user->id, 'profile');

        /*
         * If the password or email has been updated, reauthenticate the user
         */
        $emailChanged = $this->user->email != post('User[email]');

        $passwordChanged = strlen(post('User[password]'));

        if ($emailChanged || $passwordChanged)
        {
            BackendAuth::login($this->user->reload(), true);
        }

        return $result;
    }

    public function formExtendFields($form)
    {
        $fields = $form->getTabs()->primary->fields;

        $profileFields = array_only($fields, 'vitae.vita::lang.profile.model.title');

        $form->getTabs()->primary->fields = $profileFields;

        // Remove is_superuser field
        $form->removeField('is_superuser');
    }

    public function listExtendColumns($list)
    {
        $list->removeColumn('login');
        $list->removeColumn('email');
        $list->removeColumn('first_name');
        $list->removeColumn('last_name');
        $list->removeColumn('login');
        $list->removeColumn('email');
        $list->removeColumn('groups');
        $list->removeColumn('last_login');
        $list->removeColumn('created_at');
        $list->removeColumn('updated_at');
        $list->removeColumn('is_activated');
        $list->removeColumn('is_superuser');

        $list->getColumn('full_name')->invisible = false;

        $list->addColumns([
            'profile[salutation]' => [
                'label' => 'Salutation',
                'type' => 'partial',
                'path' => '~/plugins/vitae/vita/controllers/profile/_salutation_column.htm',
            ],
            'profile[zip]' => [
                'label' => 'Zip',
            ],
            'profile[city]' => [
                'label' => 'City',
            ],
        ]);
    }
}

<?php namespace Stargate\Project\Console;

use DB;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Stargate\Project\Models\Slug;
use Stargate\Gatepedia\Models\Series;

class ImportSeries extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'project:importseries';

    /**
     * @var string The console command description.
     */
    protected $description = 'Import Script for Series from SG-P to October version';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {

            #$series = DB::connection('mysql_old')->table('series')->where('id', 1)->get();
            $series = DB::connection('mysql_old')->table('series')->get();

            DB::table('stargate_gatepedia_series')->truncate();
            DB::table('stargate_project_sluggable')->where('sluggable_type', 'Stargate\Gatepedia\Models\Series')->delete();

            foreach ($series as $item) {

                $slug_old = DB::connection('mysql_old')->table('slugs')->where('sluggable_type', 'SGProject\Models\Series')->where('sluggable_id', $item->id)->first();

                $slug_new = new Slug();
                $slug_new->slug = $slug_old->slug;
                $slug_new->sluggable_id = $slug_old->sluggable_id;
                $slug_new->sluggable_type = 'Stargate\Gatepedia\Models\Series';

                $new = new Series();

                unset($new->rules['sluggable']);

                $new->id = $item->id;
                $new->title = $item->title;
                $new->description = $item->description;
                $new->teasertext = $item->teasertext;
                $new->cnt_seasons = $item->cnt_seasons;
                $new->cnt_episodes = $item->cnt_episodes;
                $new->cnt_films = $item->cnt_films;
                $new->cnt_films_unpublished = $item->cnt_films_unpublished;
                $new->broadcast_first = $item->broadcast_first != '0000-00-00 00:00:00' ? substr($item->broadcast_first, 0, 10) : NULL;
                $new->broadcast_last = $item->broadcast_last != '0000-00-00 00:00:00' ? substr($item->broadcast_last, 0, 10) : NULL;
                $new->broadcast_first_de = $item->broadcast_first_de != '0000-00-00 00:00:00' ? substr($item->broadcast_first_de, 0, 10) : NULL;
                $new->broadcast_last_de = $item->broadcast_last_de != '0000-00-00 00:00:00' ? substr($item->broadcast_last_de, 0, 10) : NULL;
                $new->image_id = $item->image_id;
                $new->sort_order = $item->id;
                $new->active = $item->active;
                $new->created_by = $item->created_by;
                $new->updated_by = $item->updated_by;
                $new->deleted_by = $item->deleted_by;
                $new->created_at = $item->created_at;
                $new->updated_at = $item->updated_at;
                $new->deleted_at = $item->deleted_at;
                $new->slug = $slug_new;
                $new->save();
            }
        });

        $this->output->writeln('Series imported');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}

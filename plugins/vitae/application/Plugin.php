<?php namespace Vitae\Application;

use Yaml;
use Event;
use View;
use Request;
use Backend;
use BackendAuth;
use Cms\Classes\Theme;
use System\Classes\PluginBase;
use Vitae\Application\Models\Application;
use Vitae\Application\Models\Action;
use Vitae\Application\Models\Preference;
use Vitae\Vita\Models\Attachment;
use Vitae\Application\Components\MyVita;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

/**
 * Application Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
    	// Adds preferences fields to Application form
    	Event::listen('backend.form.extendFields', function ($widget)
    	{
    		if (!$widget->model instanceof Application)
            {
    			return;
            }

    		$yaml_fields = Yaml::parseFile(plugins_path().'/vitae/application/models/preference/fields.yaml');

            foreach ($yaml_fields['tabs']['fields'] as $yaml_key => $yaml_field)
            {
                if ($yaml_field['type'] != 'section' && $yaml_field['type'] != 'themeselector')
                {
                    $widget->addTabFields([
                        'preferences['.$yaml_key.']' => [
                            'label' => $yaml_field['label'],
                            'type' => $yaml_field['type'],
                            'default' => $yaml_field['default'],
                            'span' => $yaml_field['span'],
                            'tab' => $yaml_field['tab'],
                        ],
                    ]);
                }
            }

            $attachments = Attachment::where('user_id', BackendAuth::getUser()->id)->get();

            foreach ($attachments as $attachment)
            {
                $widget->addTabFields([
                    'preferences[show_att_'.$attachment->id.']' => [
                        'label' => 'Show '.$attachment->title,
                        'type' => 'switch',
                        'default' => true,
                        'span' => 'auto',
                        'tab' => 'Attachments',
                    ],
                ]);
            }

            if ($widget->context == 'update')
            {
                $preferences = Preference::where('namespace', 'backend')
                    ->where('group', 'application')
                    ->where('item', 'preferences')
                    ->where('user_id', $widget->model->user_id)
                    ->first();

                $selected_theme = $preferences->theme ? $preferences->theme : null;

                Session::put('selected_theme', $selected_theme);
            }
    	});

        Event::listen('application.mailsend', function($application)
        {
            $action = new Action();
            $action->action = 'mailsend';
            $action->application_id = $application->id;
            $action->save();
        });

        Event::listen('application.login', function($application)
        {
            $action = new Action();
            $action->action = 'login';
            $action->application_id = $application->id;
            $action->save();
        });

        // Theme switcher for MyVita
        Event::listen('cms.theme.getActiveTheme', function() {
            // check if URL contains "myvita/"
            $is_myvita = starts_with(Request::path(), 'myvita/');

            if ($is_myvita) {
                $application = Application::where('applicationurl', Request::segment(2))->first();

                if ($application && (new MyVita())->checkAccess($application)) {
                    $preference = Preference::where('namespace', 'backend')
                        ->where('group', 'application')
                        ->where('item', 'preferences')
                        ->where('user_id', $application->user->id)
                        ->first();

                    if (Theme::exists($preference->theme)) {
                        return $preference->theme;
                    }
                }
            }

            // check if URL contains "preview"
            $isPreview = starts_with(Request::path(), 'preview');

            if ($isPreview && input('theme')) {
                if (Theme::exists(input('theme'))) {
                    $theme = Theme::load(input('theme'));

                    if (!$theme->hasCustomData() || array_key_exists(input('color'), $theme->getConfigArray('form.fields.color.options'))) {
                        return input('theme');
                    }
                }
            }
        });
    }

    public function registerComponents()
    {
        return [
            'Vitae\Application\Components\MyVita' => 'myVita',
            'Vitae\Application\Components\ThemePreview' => 'themePreview',
        ];
    }

    public function registerFormWidgets()
    {
        return [
            'Vitae\Application\FormWidgets\ThemeSelector' => 'themeselector',
        ];
    }

}

<?php

return [
    'plugin' => [
        'name' => 'Octobase Backend',
        'description' => 'Backend Einstellungen und nützliche AddOns',
        'backend_settings' => 'Octobase', 
    ],
    'settings' => [
        'model' => [
            'name' => 'Einstellung',
            'title' => 'Einstellungen',
            'description' => 'Generelle Backend Einstellungen bearbeiten',
        ],
        'tabs' => [
            'backend' => 'Backend',
        ],
        'labels' => [
            'settings_one_column' => 'Zeige Backend Settings 1-spaltig',
        ],
    ],
    'lists' => [
        'labels' => [
            'yes' => 'Ja',
            'no' => 'Nein',
            'created_at' => 'Erstellt',
            'updated_at' => 'Geändert',
            'sort_order' => 'Sortierung',
        ],
    ],
];
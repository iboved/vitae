<?php namespace Vitae\Vita\Models;

use Model;
use Session;
use BackendAuth;
use Vitae\Account\Traits\ModelEventTrait;
use Vitae\Vita\Classes\Helper;

/**
 * Education Model
 */
class Education extends Model
{

    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\Sortable;

    use ModelEventTrait;

    const SORT_ORDER = 'sort_order';

    /*
     * Validation
     */
    public $rules = [
        'school' => 'required',
    ];

    /**
     * Attribute names
     */
    public $attributeNames = [
        'school' => 'vitae.vita::lang.educations.labels.school',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'vitae_vita_educations';

    /**
     * User relation to show user details in lists
     */
    public $belongsTo = [
        'user' => ['Backend\Models\User'],
    ];

    /**
     * Sets user_id to current logged in user or selected user by Admin
     */
    public function beforeCreate()
    {
        if (BackendAuth::getUser()->is_superuser && Session::has('user_id'))
        {
            $this->user_id = Session::get('user_id');
        } 
        else
        {
            $this->user_id = BackendAuth::getUser()->id;
        }
    }

    public function getMonthFromOptions()
    {
        return Helper::months();
    }

    public function getYearFromOptions()
    {
        return Helper::years();
    }

    public function getMonthTillOptions()
    {
        return Helper::months();
    }

    public function getYearTillOptions()
    {
        return Helper::years();
    }
}
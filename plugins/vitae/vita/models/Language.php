<?php namespace Vitae\Vita\Models;

use Model;
use Session;
use BackendAuth;
use Lang;
use Config;
use Validator;
use Vitae\Vita\Classes\Helper;
use Vitae\Account\Traits\ModelEventTrait;

/**
 * Language Model
 */
class Language extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\Sortable;

    use ModelEventTrait;
    
    const SORT_ORDER = 'sort_order';
    
    /*
     * Validation
     */
    public $rules = [
        'locale' => 'required',
        'level' => 'required',
    ];

    /**
     * Attribute names
     */
    public $attributeNames = [
        'locale' => 'vitae.vita::lang.languages.labels.locale',
        'level' => 'vitae.vita::lang.languages.labels.level',
    ];

    /**
     * Custom messages
     */
    public $customMessages = [];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'vitae_vita_languages';

    /**
     * User relation to show user details in lists
     */
    public $belongsTo = [
        'user' => ['Backend\Models\User'],
    ];

    /**
     * Sets Session Variable to Validation rule and ovrewrite Custom messages
     */
    public function beforeValidate()
    {
        if (BackendAuth::getUser()->is_superuser && Session::has('user_id'))
        {
            $user_id = Session::get('user_id');
        } 
        else
        {
            $user_id = BackendAuth::getUser()->id;
        }

        $this->rules['locale'] = 'required|unique:vitae_vita_languages,locale,NULL,id,user_id,'.$user_id;

        $this->customMessages['locale.unique'] = 'vitae.vita::lang.validation.locale_unique';
    }


    /**
     * Sets user_id to current logged in user or selected user by Admin
     */
    public function beforeCreate()
    {
        if (BackendAuth::getUser()->is_superuser && Session::has('user_id'))
        {
            $this->user_id = Session::get('user_id');
        } 
        else
        {
            $this->user_id = BackendAuth::getUser()->id;
        }
    }

    /**
     * Returns available options for the "locale" attribute.
     * @return array
     */
    public function getLocaleOptions()
    {
        return Helper::locales();
    }

    public function getLocaleLabelAttribute()
    {
        $value = array_get($this->attributes, 'locale');

        return Lang::get('system::lang.locale.' . $value);
    }

    public function getLevelOptions()
    {
        return Helper::languagelevels();
    }

    public function getLevelLabelAttribute()
    {
        $value = array_get($this->attributes, 'level');

        return trans(array_get($this->getLevelOptions(), $value));
    }

}
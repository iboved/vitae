<?php namespace Vitae\Vita\Models;

use Lang;
use Config;
use Model;
use Backend\Models\Preference;
use Vitae\Vita\Classes\Helper;
use BackendAuth;
use Session;

/**
 * Profile Model
 */
class Profile extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'vitae_vita_profiles';

    /*
     * Validation
     */
    public $rules = [
        'street' => 'required',
        'zip' => 'required',
        'city' => 'required',
        'country' => 'required',
    ];

    protected $dates = ['birthday'];

    public $implement = ['RainLab.Location.Behaviors.LocationModel'];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'user' => ['Backend\Models\User'],
    ];

    /**
     * Sets user_id to current logged in user or selected user by Admin
     */
    public function beforeCreate()
    {
        if (BackendAuth::getUser()->is_superuser && Session::has('user_id'))
        {
            $this->user_id = Session::get('user_id');
        } 
        else
        {
            $this->user_id = BackendAuth::getUser()->id;
        }
    }

    public static function getFromUser($user)
    {
        if($user->profile)
            return $user->profile;

        $profile = new static;
        $profile->user = $user;
        $profile->save();

        $user->profile = $profile;

        return $profile;
    }

    public function getSalutationOptions()
    {
        return Helper::salutations();
    }

    public function getFamilystatusOptions()
    {
        return Helper::familystatus();
    }

    public function getDenominationOptions()
    {
        return Helper::denominations();
    }

}

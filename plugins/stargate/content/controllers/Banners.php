<?php namespace Stargate\Content\Controllers;

use BackendMenu;
use Octobase\Advertising\Controllers\Banners as BaseBanners;

/**
 * Banners Back-end Controller
 */
class Banners extends BaseBanners
{
    public function __construct()
    {
        parent::__construct();

        $this->addViewPath(plugins_path('octobase/advertising/controllers/banners'));
        $this->viewPath = array_reverse($this->viewPath);

        BackendMenu::setContext('Stargate.Content', 'content', 'banners');
    }
}

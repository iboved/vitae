<?php namespace Kay899\Band\Models;

use Model;

/**
 * Appointment Model
 */
class Appointment extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \Kay899\Band\Traits\Base;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kay899_band_appointments';

    /**
     * Validation rules
     */
    public $rules = [
        'title' => 'required',
        'type' => 'required',
        'date_from' => 'required',
    ];

    /**
     * @var array Date fields
     */
    protected $dates = ['date_from', 'date_till'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'gig' => [
            'Kay899\Band\Models\Gig',
        ]
    ];

    public function beforeValidate()
    {
        if ($this->type === 'absence') {
            $this->rules['date_till'] = 'required';
        }
    }

    public static function getTypeOptions()
    {
        return [
            'rehearsal' => 'kay899.band::lang.appointments.types.rehearsal',
            'event'     => 'kay899.band::lang.appointments.types.event',
            'absence'   => 'kay899.band::lang.appointments.types.absence',
        ];
    }

    public function getTypeLabelAttribute()
    {
        $value = array_get($this->attributes, 'type');

        return trans(array_get($this->getTypeOptions(), $value));
    }
}

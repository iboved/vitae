<?php namespace Kay899\Band\Models;

use Model;
use Storage;
use October\Rain\Database\Traits\Sortable;
use Session;
use Kay899\Band\Models\Gig;

/**
 * Song Model
 */
class Song extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \Kay899\Band\Traits\Base;

    use Sortable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kay899_band_songs';

    /**
     * Validation rules
     */
    public $rules = [
        'title' => 'required',
        'artist' => 'required',
    ];

    /**
     * Action relation to show action details in lists
     */
    public $hasMany = [
        'links' => 'Kay899\Band\Models\Link',
        'links_count' => ['Kay899\Band\Models\Link', 'count' => true]
    ];

    public $morphedByMany = [
        'profiles'  => ['Kay899\Band\Models\Profile', 'name' => 'songable']
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['title'];

    public static function getStatusOptions()
    {
        return [
            '1' => 'kay899.band::lang.songs.status.1',
            '2' => 'kay899.band::lang.songs.status.2',
            '3' => 'kay899.band::lang.songs.status.3',
        ];
    }

    public function getStatusLabelAttribute()
    {
        $value = array_get($this->attributes, 'status');

        return trans(array_get($this->getStatusOptions(), $value));
    }

    public function getTopicOptions()
    {
        return [
            '1' => 'kay899.band::lang.songs.topics.1',
            '2' => 'kay899.band::lang.songs.topics.2',
            '3' => 'kay899.band::lang.songs.topics.3',
        ];
    }

    public function getTopicLabelAttribute()
    {
        $value = array_get($this->attributes, 'topic');

        return trans(array_get($this->getTopicOptions(), $value));
    }

    public function getTextLabelAttribute()
    {
        $value = array_get($this->attributes, 'text');

        $text = strlen($value) > 0 ? trans('backend::lang.list.column_switch_true') : '';

        return $text;
    }

    public function beforeSave()
    {
        // we edit Song directly
        if (!post('_relation_field')) {
            // Genererate file from former song name
            $filename = str_slug($this->getOriginal('title'), '_').'.txt';

            if (Storage::exists('setlist/'.$filename))
            {
                Storage::delete('setlist/'.$filename);
            }

            if (Storage::disk('dropbox')->exists($filename))
            {
                Storage::disk('dropbox')->delete($filename);
            }
        }
    }

    public function afterSave()
    {
        // we edit Song directly
        if (!post('_relation_field')) {
            if (strlen($this->text))
            {
                // Genererate file from current song name
                $filename = str_slug($this->title, '_').'.txt';

                // Store filename for later use
                $file = $filename;

                // Generate new file with song title on 
                Storage::put($file, $this->title);

                // Append Artist
                Storage::append($file, 'Artist: '. $this->artist);

                // Append Topic Label
                Storage::append($file, 'Topic: '. $this->topic_label .', ' . $this->status_label);

                // Append Tempo, if exist
                if (mb_strlen($this->tempo))
                {
                    Storage::append($file, 'Tempo: '. $this->tempo);
                }

                // Append First Key, if exist
                if (mb_strlen($this->first_key))
                {
                    Storage::append($file, 'Key: '. $this->first_key);
                }

                // Append needed empty line
                Storage::append($file, '');

                // Append Song Text
                Storage::append($file, $this->text);

                // Save contents for later use
                $contents = Storage::get($file);

                // Copy local file to Dropbox App
                Storage::disk('dropbox')->put($filename, $contents);

                // Move file to local setlist folder
                Storage::move($file, 'setlist/'.$file);
            }
        }
    }

    public function getTitleStatusAttribute()
    {
        return $this->title . ' (' . $this->status_label . ')';
    }

    public static function listSetlistOptions()
    {
        $url_segments = explode("/", Session::get('_previous')['url']);
        $sets = Gig::find(end($url_segments))->sets;

        $options = [];

        for ($i=1; $i <= $sets; $i++) {
            $options[$i] = trans('kay899.band::lang.gigs.labels.set') . ' ' . $i;
        }

        $options[99] = trans('kay899.band::lang.gigs.labels.add_ons');

        return $options;
    }

}
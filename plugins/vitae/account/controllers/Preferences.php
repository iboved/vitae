<?php namespace Vitae\Account\Controllers;

use Backend;
use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Support\Facades\Lang;
use October\Rain\Support\Facades\Flash;
use System\Classes\SettingsManager;
use Backend\Models\Preference as PreferenceModel;

/**
 * Preferences Back-end Controller
 */
class Preferences extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
    ];

    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();

        $this->pageTitle = 'vitae.account::lang.preferences.model.title';

        $this->addJs('/modules/backend/assets/js/preferences/preferences.js', 'core');

        BackendMenu::setContext('Vitae.Account', 'account', 'preferences');

        SettingsManager::setContext('October.Backend', 'preferences');
    }

    public function index()
    {
        $this->asExtension('FormController')->update();
    }

    public function formFindModelObject()
    {
        return PreferenceModel::instance();
    }

    public function index_onSave()
    {
        return $this->asExtension('FormController')->update_onSave();
    }
}

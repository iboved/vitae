<?php namespace Vitae\Account\Skins;

use Backend\Classes\Skin;
use Backend\Controllers\Auth;

class BackendLogin extends Skin
{
    public function __construct()
    {
        Auth::extend(function (Auth $controller)
        {
            $controller->addViewPath('$/vitae/account/controllers/accountauth');
        });

        parent::__construct();
    }

    public function skinDetails()
    {
        return [
            'name' => 'BackendLogin Skin'
        ];
    }
}

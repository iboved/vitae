<?php namespace Stargate\Project\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCategorizeableTable extends Migration
{
    public function up()
    {
        Schema::create('categorizeables', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('category_id')->nullable();
            $table->integer('categorizeable_id')->nullable();
            $table->string('categorizeable_type', 255)->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('categorizeables');
    }
}

<?php namespace Kay899\Band\Components;

use Cms\Classes\ComponentBase;
use Kay899\Band\Models\Song;

class Probenzettel extends ComponentBase
{
    /**
     * Array of setlist
     * @var array
     */
    public $probenzettel;

    public function componentDetails()
    {
        return [
            'name'        => 'Probenzettel Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun()
    {
        $suggestedSongs = Song::where('status', 1)->orderBy('sort_order', 'asc')->get();
        $inProgressSongs = Song::where('status', 2)->orderBy('sort_order', 'asc')->get();
        $setlistSongs = Song::where('status', 3)->orderBy('sort_order', 'asc')->get();

        $this->suggestedSongs = $this->page['suggestedSongs'] = $suggestedSongs;
        $this->inProgressSongs = $this->page['inProgressSongs'] = $inProgressSongs;
        $this->setlistSongs = $this->page['setlistSongs'] = $setlistSongs;
    }
}

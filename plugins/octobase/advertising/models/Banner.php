<?php namespace Octobase\Advertising\Models;

use Model;
use Octobase\Backend\Traits\Base;
use October\Rain\Database\Traits\SoftDelete;
use October\Rain\Database\Traits\Validation;
use Octobase\Backend\Models\Setting;

/**
 * Banner Model
 */
class Banner extends Model
{
    use Validation, SoftDelete, Base;

    public $table = 'octobase_advertising_banners';

    public $rules = [
        'title' => 'required',
        'position' => 'required',
        'html_code' => 'required',
    ];

    public $attributeNames = [
        'title' => 'octobase.advertising::lang.banners.labels.title',
        'position' => 'octobase.advertising::lang.banners.labels.position',
        'html_code' => 'octobase.advertising::lang.banners.labels.html_code',
    ];

    public function getPositionOptions()
    {
        $position_settings = Setting::get('advertising_positions');

        $position_list = explode(',', $position_settings);

        $positions = [];

        foreach ($position_list as $value)
        {
            $positions[trim($value)] = trim($value);
        }

        return $positions;
    }

}

<?php namespace Kay899\Band\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGigsSongsTable extends Migration
{
    public function up()
    {
        Schema::create('kay899_band_gigs_songs', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('gig_id')->unsigned();
            $table->integer('song_id')->unsigned();
            $table->tinyInteger('set')->nullable();
            $table->integer('gig_sort_order')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('gig_id')->references('id')->on('kay899_band_gigs')->onDelete('cascade');
            $table->foreign('song_id')->references('id')->on('kay899_band_songs')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('kay899_band_gigs_songs');
    }
}

<?php namespace Vitae\Account\Behaviors;

use Backend;
use BackendAuth;
use Session;
use ApplicationException;
use Backend\Classes\ControllerBehavior;

class MultiTenantController extends ControllerBehavior
{
    /**
     * @var \Backend\Classes\Controller|FormController Reference to the back end controller.
     */
    protected $controller;

    /**
     * Behavior constructor
     * @param Backend\Classes\Controller $controller
     */
    public function __construct($controller)
    {
        parent::__construct($controller);
    }

    // Removes Client Name for non SuperUsers
    public function listExtendColumns($list)
    {
        if (!BackendAuth::getUser()->is_superuser)
        {
            $list->removeColumn('user_id');
        }
    }

    // Filters lists for SuperUsers selection
    public function listExtendQuery($query)
    {
        if (BackendAuth::getUser()->is_superuser)
        {
            if (Session::has('user_id') && Session::get('user_id') != 0)
            {
                $query->where('user_id', Session::get('user_id'));
            }
        } 
        else
        {
            $query->where('user_id', BackendAuth::getUser()->id);
        }
    }

    // Filters reorder lists for SuperUsers selection
    public function reorderExtendQuery($query)
    {
        if (BackendAuth::getUser()->is_superuser)
        {
            if (Session::has('user_id') && Session::get('user_id') != 0)
            {
                $query->where('user_id', Session::get('user_id'));
            }
        } 
        else
        {
            $query->where('user_id', BackendAuth::getUser()->id);
        }
    }
}
<?php namespace Octobase\Backend\Models;

use Model;

/**
 * Setting Model
 */
class Setting extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'octobase-backend-settings';

    public $settingsFields = 'fields.yaml';
}
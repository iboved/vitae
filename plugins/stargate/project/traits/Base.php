<?php namespace Stargate\Project\Traits;

use BackendAuth;
use Backend\Models\User;

trait Base
{
    public function beforeCreate()
    {
        if (php_sapi_name() != 'cli') {
            $this->created_by = BackendAuth::getUser()->id;
            $this->updated_by = BackendAuth::getUser()->id;
        }
    }

    public function beforeUpdate($id = null)
    {
        $this->updated_by = BackendAuth::getUser()->id;
    }

    public function beforeDelete($id = null)
    {
        $this->updated_by = BackendAuth::getUser()->id;
        $this->deleted_by = BackendAuth::getUser()->id;
        $this->save();
    }

    public function getCreatedSlugAttribute()
    {
        $user = User::find($this->created_by);

        return $user->login;
    }

    public function getUpdatedSlugAttribute()
    {
        $user = User::find($this->updated_by);

        return $user->login;
    }
}
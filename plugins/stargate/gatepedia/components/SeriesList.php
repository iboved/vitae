<?php namespace Stargate\Gatepedia\Components;

use Cms\Classes\ComponentBase;
use Stargate\Gatepedia\Models\Series;

class SeriesList extends ComponentBase
{

    /**
     * Array of seriesList
     * @var array
     */
    public $seriesList;

    /**
     * ListColumns the component is rendered as
     * @var string
     */
    public $listColumns;

    public function componentDetails()
    {
        return [
            'name'        => 'Gatepedia Serien',
            'description' => 'Übersicht aller Serien'
        ];
    }

    public function defineProperties()
    {
        return [
            'listColumns' => [
                'title'        => 'Anzahl Spalten',
                'description'  => 'Hier kann man die Anzahl der Spalten im Layout angeben',
                'type'         => 'dropdown',
                'options'      => ['2'=>'2', '3'=>'3', '4'=>'4'],
                'showExternalParam' => false
            ]
        ];
    }

    // This array becomes available on the page as {{ component.posts }}
    public function series()
    {
        return Series::where('active', 1)->get();
    }

    public function onRun()
    {
        $this->listColumns = trim($this->property('listColumns'));
    }
}

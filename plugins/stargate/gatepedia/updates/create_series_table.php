<?php namespace Stargate\Gatepedia\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSeriesTable extends Migration
{
    public function up()
    {
        Schema::create('stargate_gatepedia_series', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->text('teasertext')->nullable();
            $table->smallInteger('cnt_seasons')->default(0)->nullable();
            $table->smallInteger('cnt_episodes')->default(0)->nullable();
            $table->smallInteger('cnt_films')->default(0)->nullable();
            $table->smallInteger('cnt_films_unpublished')->default(0)->nullable();
            $table->date('broadcast_first')->nullable();
            $table->date('broadcast_last')->nullable();
            $table->date('broadcast_first_de')->nullable();
            $table->date('broadcast_last_de')->nullable();
            $table->integer('image_id')->nullable();
            $table->integer('sort_order')->default(0);
            $table->boolean('active')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('stargate_gatepedia_series');
    }
}

<?php namespace Stargate\Gatepedia\Models;

use Model;

/**
 * Model
 */
class Series extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \October\Rain\Database\Traits\Sortable;

    use \Stargate\Project\Traits\Sluggable;

    const SORT_ORDER = 'sort_order';

    protected $dates = ['deleted_at'];

    protected $sluggableModel = 'Series';

    /**
     * Validation rules
     */
    public $rules = [
        'title' => 'required',
        'description' => 'required',
        'sluggable' => 'required|unique:stargate_project_sluggable,slug',
    ];

    /**
     * Attribute names
     */
    public $attributeNames = [
        'title' => 'Titel',
        'description' => 'Beschreibung',
        'sluggable' => 'Slug',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'stargate_gatepedia_series';

    /**
     * Relations
     */

    public $morphMany = [
        'slug' => ['Stargate\Project\Models\Slug', 'name' => 'sluggable'],
    ];

    public $belongsTo = [
        'image' => ['Octobase\Mediamanager\Models\Image'],
    ];

}
<?php namespace Vitae\Account\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Illuminate\Support\Facades\Session;
use Vitae\Vita\Models\Interest;
use Vitae\Vita\Models\Skill;
use Vitae\Vita\Models\Qualification;
use Vitae\Vita\Models\Award;
use Vitae\Vita\Models\Membership;
use Vitae\Vita\Models\Language;
use Vitae\Vita\Models\Education;
use Vitae\Vita\Models\Experience;
use Vitae\Vita\Models\Attachment;

/**
 * Status Form Widget
 */
class Status extends ReportWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'vitae_account_status';

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();

        return $this->makePartial('status');
    }

    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'Widget Title',
                'default'           => 'Vitae Status',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'The Widget Title is required.'
            ]
        ];
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['interests'] = Session::get('status_widget.' . Interest::class);
        $this->vars['skills'] = Session::get('status_widget.' . Skill::class);
        $this->vars['qualifications'] = Session::get('status_widget.' . Qualification::class);
        $this->vars['awards'] = Session::get('status_widget.' . Award::class);
        $this->vars['memberships'] = Session::get('status_widget.' . Membership::class);
        $this->vars['languages'] = Session::get('status_widget.' . Language::class);
        $this->vars['educations'] = Session::get('status_widget.' . Education::class);
        $this->vars['experiences'] = Session::get('status_widget.' . Experience::class);
        $this->vars['attachments'] = Session::get('status_widget.' . Attachment::class);
    }

}

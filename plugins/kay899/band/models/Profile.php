<?php namespace Kay899\Band\Models;

use Model;
use Backend\Models\User as BackendUserModel;

/**
 * Profile Model
 */
class Profile extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \Kay899\Band\Traits\Base;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kay899_band_profiles';

    /*
     * Validation
     */
    public $rules = [
        'facebook' => 'url',
    ];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'user' => 'Backend\Models\User',
    ];

    public $attachOne = [
        'teaser' => 'System\Models\File'
    ];

    public $morphToMany = [
        'songs' => ['Kay899\Band\Models\Song', 'name' => 'songable']
    ];

    public function getUserIdLabelAttribute()
    {
        $value = array_get($this->attributes, 'user_id');

        $first_name = BackendUserModel::find($value)->first_name;

        return $first_name;
    }

}

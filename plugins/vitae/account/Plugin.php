<?php namespace Vitae\Account;

use Backend;
use BackendAuth;
use Event;
use Illuminate\Support\Facades\Session;
use Backend\Models\User as BackendUserModel;
use Backend\Controllers\Users as BackendUsersController;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use System\Controllers\Settings;
use Vitae\Account\Controllers\MyAccount;
use Vitae\Vita\Models\Attachment;
use Vitae\Vita\Models\Award;
use Vitae\Vita\Models\Education;
use Vitae\Vita\Models\Experience;
use Vitae\Vita\Models\Interest;
use Vitae\Vita\Models\Language;
use Vitae\Vita\Models\Membership;
use Vitae\Vita\Models\Qualification;
use Vitae\Vita\Models\Skill;

/**
 * Account Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        BackendUserModel::extend(function($model)
        {
            // Ensures that login field always has the same value as email
            $model->bindEvent('model.beforeValidate', function () use ($model)
            {
                if ($model->login !== $model->email)
                {
                    $model->login = $model->email;
                }
            });
        });

        // Set Status Widget Session data after login
        Event::listen('backend.user.login', function ()
        {
            $this->setStatusWidgetData(BackendAuth::getUser());
        });

        Event::listen('backend.page.beforeDisplay', function($controller, $action, $params)
        {
            // Set Status Widget Session data
            if (BackendAuth::check() && !Session::has('status_widget'))
            {
                $this->setStatusWidgetData(BackendAuth::getUser());
            }

            // Add CSS to disable Status Widget edit functions for non SuperUsers
            if (BackendAuth::check() && !BackendAuth::getUser()->hasAccess('vitae.account.widget'))
            {
                $controller->addCss('/plugins/vitae/account/assets/css/widgets.css');
            }

            // Load account view into Profile 
            BackendUsersController::extend(function ($controller)
            {
                $controller->addViewPath('$/vitae/account/controllers/users');
            });

            // Load session data into onboardx JS
            $controller->addJs('/plugins/vitae/account/assets/js/onboardx.js', [
                'id' => 'user',
                'data-name' => Session::get('status_widget.name'),
                'data-email' => Session::get('status_widget.email'),
                'data-signed_up' => Session::get('status_widget.created_at'),
                'data-interests' => Session::get('status_widget.' . Interest::class),
                'data-skills' => Session::get('status_widget.' . Skill::class),
                'data-qualifications' => Session::get('status_widget.' . Qualification::class),
                'data-awards' => Session::get('status_widget.' . Award::class),
                'data-memberships' => Session::get('status_widget.' . Membership::class),
                'data-languages' => Session::get('status_widget.' . Language::class),
                'data-educations' => Session::get('status_widget.' . Education::class),
                'data-experiences' => Session::get('status_widget.' . Experience::class),
                'data-attachments' => Session::get('status_widget.' . Attachment::class),
            ]);
        });

        // Hide Settings area for nun SuperUsers
        Event::listen('backend.menu.extendItems', function($manager)
        {
            if (!BackendAuth::getUser()->is_superuser) {
                $manager->removeMainMenuItem('October.System', 'system');
            }
        });

        Event::listen('backend.form.extendFields', function ($widget)
        {
            if (!$widget->getController() instanceof MyAccount)
            {
                return;
            }

            // Remove the Group Tab
            $widget->removeTab('backend::lang.user.groups');

            // Ensures that login fields is not changeable
            $widget->getField('login')->readOnly = true;

        });

        // Settings navigation show only 1 column a start (build 420 waste)
        Settings::extend(function ($controller)
        {
            $controller->addJs('/plugins/vitae/account/assets/js/settings.js');
        });

        // Prepare mainmenu-accountmenu menue items for non SuperUsers
        SettingsManager::instance()->registerCallback(function ($manager) 
        {
            if (!BackendAuth::getUser()->is_superuser)
            {
                $manager->registerSettingItems('October.Backend', [
                    'my_account' => [
                        'label'       => 'vitae.account::lang.myaccount.model.title',
                        'category'    => SettingsManager::CATEGORY_MYSETTINGS,
                        'icon'        => 'icon-cloud',
                        'url'         => Backend::url('vitae/account/myaccount'),
                        'order'       => 100,
                        'context'     => 'mysettings',
                    ],
                    'my_preferences' => [
                        'label'       => 'vitae.account::lang.preferences.model.title',
                        'category'    => SettingsManager::CATEGORY_MYSETTINGS,
                        'icon'        => 'icon-cogs',
                        'url'         => Backend::url('vitae/account/preferences'),
                        'order'       => 110,
                        'context'     => 'mysettings'
                    ],
                ]);
            }
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Vitae\Account\Components\Register' => 'register',
        ];
    }

    public function registerMailTemplates()
    {
        return [
            'vitae.account::mail.welcome' => 'Welcome email sent when a user is registered',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'vitae.account.config_permission' => [
                'tab' => 'Account',
                'label' => 'Account Settings'
            ],
            'vitae.account.widget' => [
                'tab' => 'Account',
                'label' => 'Allow to widgets settings'
            ],
        ];
    }

    /**
     * Registers setting items for this plugin.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Account Settings',
                'description' => 'Account based settings.',
                'category'    => 'Vitae',
                'icon'        => 'icon-cog',
                'class'       => 'Vitae\Account\Models\Setting',
                'permissions' => ['vitae.account.config_permission'],
                'order'       => 1,
            ]
        ];
    }

    public function registerReportWidgets()
    {
        return [
            'Vitae\Account\ReportWidgets\Status' => [
                'label'   => 'Vitae Status',
                'context' => 'vitae'
            ],
        ];
    }

    public function setStatusWidgetData($user)
    {
        $user = BackendAuth::getUser();

        Session::put('status_widget', [
            'name' => $user->full_name,
            'email' => $user->email,
            'created_at' => $user->created_at,
            Interest::class => Interest::where('user_id', $user->id)->count(),
            Skill::class => Skill::where('user_id', $user->id)->count(),
            Qualification::class => Qualification::where('user_id', $user->id)->count(),
            Award::class => Award::where('user_id', $user->id)->count(),
            Membership::class => Membership::where('user_id', $user->id)->count(),
            Language::class => Language::where('user_id', $user->id)->count(),
            Education::class => Education::where('user_id', $user->id)->count(),
            Experience::class => Experience::where('user_id', $user->id)->count(),
            Attachment::class => Attachment::where('user_id', $user->id)->count(),
        ]);
    }
}

<?php namespace Octobase\MediaManager\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateImagesTable extends Migration
{
    public function up()
    {
        Schema::create('octobase_mediamanager_images', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('file_name');
            $table->string('file_url');
            $table->string('file_size');
            $table->string('content_type');
            $table->integer('folder_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('octobase_mediamanager_images');
    }
}

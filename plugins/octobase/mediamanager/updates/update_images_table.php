<?php namespace Stargate\Project\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateImagesTable extends Migration
{
    public function up()
    {
        Schema::table('octobase_mediamanager_images', function(Blueprint $table) {
            $table->text('copyright')->after('content_type')->nullable();
        });
    }

    public function down()
    {
        Schema::table('octobase_mediamanager_images', function(Blueprint $table) {
            $table->dropColumn('copyright');
        });
    }
}

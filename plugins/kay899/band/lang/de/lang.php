<?php 

return [
    'plugin' => [
        'name' => 'Rockability',
        'description' => '',
    ],
    'songs' => [
        'model' => [
            'name' => 'Song',
            'title' => 'Songs',
            'create' => 'Song erstellen',
            'update' => 'Song ändern',
            'reorder' => 'Songs sortieren',
        ],
        'labels' => [
            'title' => 'Titel',
            'artist' => 'Künstler',
            'internal_remark' => 'Interne Bemerkung',
            'duration_orig' => 'Dauer Orig.',
            'duration_rab' => 'Dauer rAb',
            'first_key' => 'Erster Chord',
            'tempo' => 'Tempo',
            'text' => 'Text/Bemerkungen',
            'topic' => 'Topic',
            'status' => 'Status',
            'is_public' => 'Öffentlich',
            'preview' => 'Text-Vorschau',
        ],
        'hints' => [
            'text' => 'Aus dem Text und den Angaben wird automatisch eine Datei für die OnSong App erzeugt und in einer DropBox gespeichert.',
            'internal_remark' => 'Diese Bemerkung wird nur auf der Internen Setlist angezeigt',
        ],
        'buttons' => [
            'preview' => 'Vorschau',
            'back' => 'Zurück',
            'printpreview' => 'Druck-Vorschau',
        ],
        'status' => [
            '1' => 'Vorschlag',
            '2' => 'Work in progress',
            '3' => 'Setlist',
        ],
        'topics' => [
            '1' => 'Duett',
            '2' => 'Linda',
            '3' => 'Andreas',
        ],
    ],
    'links' => [
        'model' => [
            'name' => 'Link',
            'title' => 'Links',
            'create' => 'Link erstellen',
            'update' => 'Link ändern',
            'reorder' => 'Links sortieren',
        ],
        'labels' => [
            'title' => 'Titel',
            'link' => 'Link',
            'user_id' => 'Wer?',
        ],
        'hints' => [
            'links' => 'Hier können Youtube, Vimeo oder andere Links (auch zu MP3/4 Dateien) gepostet werden.',
        ],
    ],
    'profiles' => [
        'model' => [
            'name' => 'Profil',
            'title' => 'Profile',
            'create' => 'Profil erstellen',
            'update' => 'Profil ändern',
            'reorder' => 'Profile sortieren',
        ],
        'labels' => [
            'position' => 'Position',
            'about_me' => 'Über mich',
            'facebook' => 'Facebook-Link',
            'favorite_songs' => 'Lieblings-Songs',
            'is_public' => 'Öffentlich',
            'user_id' => 'Name',
            'teaser' => 'Teaser-Bild',
        ],
        'hints' => [
            'songs' => 'Hier bitte 3-5 Lieblings-Songs auswählen',
        ],
    ],
    'galleries' => [
        'model' => [
            'name' => 'Galerie',
            'title' => 'Galerien',
            'create' => 'Galerie erstellen',
            'update' => 'Galerie ändern',
            'reorder' => 'Galerien sortieren',
        ],
        'labels' => [
            'title' => 'Titel',
            'text' => 'Text',
            'copyright' => 'Copyright',
            'is_public' => 'Öffentlich',
            'images' => 'Bilder',
        ],
    ],
    'galleries' => [
        'model' => [
            'name' => 'Galerie',
            'title' => 'Galerien',
            'create' => 'Galerie erstellen',
            'update' => 'Galerie ändern',
            'reorder' => 'Galerien sortieren',
        ],
        'labels' => [
            'title' => 'Titel',
            'text' => 'Text',
            'copyright' => 'Copyright',
            'is_public' => 'Öffentlich',
            'images' => 'Bilder',
        ],
    ],
    'gigs' => [
        'model' => [
            'name' => 'Auftritt',
            'title' => 'Auftritte',
            'create' => 'Auftritt erstellen',
            'update' => 'Auftritt ändern',
            'reorder' => 'Auftritte sortieren',
        ],
        'labels' => [
            'title' => 'Titel',
            'datetime' => 'Datum/Uhrzeit',
            'teasertext' => 'Teaser-Text',
            'description' => 'Beschreibung',
            'set' => 'Set',
            'sets' => 'Sets',
            'location' => 'Veranstaltungsort',
            'location_details' => 'Details VA-Ort',
            'location_link' => 'Link VA-Ort',
            'entrance_fee' => 'Eintrittspreise',
            'facebook' => 'Facebook-Link',
            'is_public' => 'Öffentlich',
            'setlist' => 'Setliste',
            'setlists' => 'Setlisten',
            'gig_sort_order' => 'Sortierung',
            'add_ons' => 'Zugaben',
        ],
        'tabs' => [
            'details' => 'Details',
            'setlists' => 'Setlisten',
        ],
        'hints' => [
            'setlists' => 'Die Setliste kann erst nach dem Erstellen eines Auftritts erzeugt werden.',
        ],
        'printpreview' => [
            'when' => 'Wann?',
            'where' => 'Wo?',
            'back' => 'Zurück',
        ],
    ],
    'appointments' => [
        'model' => [
            'name' => 'Termin',
            'title' => 'Termine',
            'create' => 'Termin erstellen',
            'update' => 'Termin ändern',
            'reorder' => 'Termine sortieren',
        ],
        'labels' => [
            'title' => 'Titel',
            'type' => 'Termin-Typ',
            'date_from' => 'Datum',
            'date_till' => 'Datum bis',
            'start_time' => 'Uhrzeit',
            'location' => 'Ort',
            'hints' => 'Hinweise',
            'gig' => 'Aufritt',
            'is_public' => 'Öffentlich',
        ],
        'types' => [
            'rehearsal' => 'Bandprobe',
            'event' => 'Event',
            'absence' => 'Abwesenheit',
        ],
        'options' => [
            'empty' => 'Bitte Auftritt wählen',
        ],
    ],
    'frontendlogin' => [
        'login' => 'Login',
        'form_password' => 'Passwort',
        'btn_login' => 'Login',
        'btn_logout' => 'Logout',
    ],
    'lists' => [
        'labels' => [
            'created_at' => 'Erstellt',
            'updated_at' => 'Geändert',
            'sort_order' => 'Sortierung',
        ],
    ],
];
<?php namespace Vitae\Account\Controllers;

use Backend\Facades\Backend;
use Backend\Facades\BackendAuth;
use Backend\Models\AccessLog;
use Backend\Classes\Controller;
use Backend\Models\User;
use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use October\Rain\Exception\ValidationException;
use October\Rain\Support\Facades\Flash;
use System\Classes\UpdateManager;

/**
 * Account Auth Back-end Controller
 */
class AccountAuth extends Controller
{
    protected $publicActions = ['signin', 'restore'];

    public function __construct()
    {
        User::$loginAttribute = 'email';

        parent::__construct();
    }

    public function signin()
    {
        try
        {
            return $this->authenticate();
        }
        catch (Exception $ex)
        {
            Flash::error($ex->getMessage());

            return Backend::redirect('backend/auth/signin')->withInput(Input::except('password'));
        }
    }

    public function authenticate()
    {
        $rules = [
            'email' => 'required|email|between:6,255',
            'password' => 'required|between:4,255',
        ];

        $validation = Validator::make(post(), $rules);

        if ($validation->fails())
        {
            throw new ValidationException($validation);
        }

        if (is_null($remember = config('cms.backendForceRemember', true)))
        {
            $remember = (bool) post('remember');
        }

        // Authenticate user
        $user = BackendAuth::authenticate([
            'email' => post('email'),
            'password' => post('password')
        ], $remember);

        // Load version updates
        UpdateManager::instance()->update();

        // Log the sign in event
        AccessLog::add($user);

        // Redirect to the intended page after successful sign in
        if (!BackendAuth::getUser()->hasAccess('backend.access_dashboard'))
        {
            return Backend::redirectIntended('backend/vitae/account/index');
        }
        else
        {
            return Backend::redirectIntended('backend');
        }
    }

    public function restore()
    {
        try
        {
            return $this->restorePassword();
        }
        catch (Exception $ex)
        {
            Flash::error($ex->getMessage());

            return Backend::redirect('backend/auth/restore')->withInput();
        }
    }

    public function restorePassword()
    {
        $rules = [
            'email' => 'required|email|between:6,255',
        ];

        $validation = Validator::make(post(), $rules);

        if ($validation->fails())
        {
            throw new ValidationException($validation);
        }

        $user = BackendAuth::findUserByLogin(post('email'));

        if (!$user)
        {
            throw new ValidationException([
                'email' => trans('vitae.account::lang.account.restore_error', ['email' => post('email')])
            ]);
        }

        Flash::success(trans('backend::lang.account.restore_success'));

        $code = $user->getResetPasswordCode();

        $link = Backend::url('backend/auth/reset/'.$user->id.'/'.$code);

        $data = [
            'name' => $user->full_name,
            'link' => $link,
        ];

        Mail::send('backend::mail.restore', $data, function ($message) use ($user)
        {
            $message->to($user->email, $user->full_name)->subject(trans('backend::lang.account.password_reset'));
        });

        return Backend::redirect('backend/auth/signin');
    }
}

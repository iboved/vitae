<?php namespace Stargate\Project\Console;

use DB;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Stargate\Project\Models\Slug;
use Stargate\Gatepedia\Models\Film;

class ImportFilms extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'project:importfilms';

    /**
     * @var string The console command description.
     */
    protected $description = 'Import Script for Films from SG-P to October version';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {

            #$films = DB::connection('mysql_old')->table('films')->where('id', 1)->get();
            $films = DB::connection('mysql_old')->table('films')->get();

            // How to Import?
            // 1. Import latest version of complete DB from SGP.sgproject to local.sgproject
            // 2. Import "stargate_project_sluggable" and "categorizeables" tables from HWS.ocsgp to local.ocsgp
            // 3. Run import on local
            // 4. Export imported Models table, "stargate_project_sluggable" and "categorizeables" from local.sgp to HWS.sgp
            // 5. Import latest version of complete DB from HWS.ocsgp to local.ocsgp_remote
            // 6. Change Model to new Table and Test
            DB::table('stargate_gatepedia_films')->truncate();
            DB::table('stargate_project_sluggable')->where('sluggable_type', 'Stargate\Gatepedia\Models\Film')->delete();
            DB::table('categorizeables')->where('categorizeable_type', 'Stargate\Gatepedia\Models\Film')->delete();

            foreach ($films as $item) {

                $slug_old = DB::connection('mysql_old')->table('slugs')->where('sluggable_type', 'SGProject\Models\Film')->where('sluggable_id', $item->id)->first();
                $category_old = DB::connection('mysql_old')->table('categorizeables')->where('categorizeable_type', 'SGProject\Models\Film')->where('categorizeable_id', $item->id)->first();

                $slug_new = new Slug();
                $slug_new->slug = $slug_old->slug;
                $slug_new->sluggable_id = $slug_old->sluggable_id;
                $slug_new->sluggable_type = 'Stargate\Gatepedia\Models\Film';

                $new = new Film();

                unset($new->rules['sluggable']);
                unset($new->rules['categories']);

                $new->id = $item->id;
                $new->title = $item->title;
                $new->originaltitle = $item->originaltitle;
                $new->description = $item->description;
                $new->teasertext = $item->teasertext;
                $new->broadcast_first = $item->broadcast_first != '0000-00-00 00:00:00' ? substr($item->broadcast_first, 0, 10) : NULL;
                $new->broadcast_first_de = $item->broadcast_first_de != '0000-00-00 00:00:00' ? substr($item->broadcast_first_de, 0, 10) : NULL;
                $new->series_id = $item->series_id;
                $new->image_id = $item->image_id;
                $new->sort_order = $item->id;
                $new->active = $item->active;
                $new->created_by = $item->created_by;
                $new->updated_by = $item->updated_by;
                $new->deleted_by = $item->deleted_by;
                $new->created_at = $item->created_at;
                $new->updated_at = $item->updated_at;
                $new->deleted_at = $item->deleted_at;
                $new->slug = $slug_new;
                $new->categories = [$category_old->category_id];
                $new->save();
            }
        });

        $this->output->writeln('Films imported');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}

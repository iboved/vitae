<?php namespace Octobase\MediaManager\Controllers;

use Backend;
use BackendMenu;
use Backend\Classes\Controller;
use Octobase\Backend\Models\Setting;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Lang;
use File;
use ApplicationException;
use October\Rain\Filesystem\Definitions as FileDefinitions;

/**
 * Images Back-end Controller
 */
class Images extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        if (Setting::get('mediamanager_show_copyright')) {
            $this->formConfig = 'config_form_copyright.yaml';
        }

        parent::__construct();

        BackendMenu::setContext('October.Backend', 'media', 'images');
    }

    public function without_title()
    {
        Session::put('widget.octobase_mediamanager-Images-Filter-listFilter', base64_encode(serialize(['scope-without_title' => true])));

        return Redirect::to(Backend::url('octobase/mediamanager/images'));
    }

    public function onUpdateForm()
    {
        $this->asExtension('FormController')->update(post('record_id'));

        $model = $this->formFindModelObject(post('record_id'));

        $extension = pathinfo($model->file_url, PATHINFO_EXTENSION);

        $this->vars['recordId'] = post('record_id');
        $this->vars['extension'] = !empty($extension) ? '.' . $extension : '';

        return $this->makePartial('form_update');
    }

    public function onUpdate()
    {
        $newFileTitle = trim(post('Image.title'));

        if (!strlen($newFileTitle)) {
            throw new ApplicationException(Lang::get('octobase.mediamanager::lang.images.validation.title_cant_be_empty'));
        }

        $newFileName = trim(post('Image.file_name'));

        if (!$this->validateFileName($newFileName)) {
            throw new ApplicationException(Lang::get('cms::lang.asset.invalid_name'));
        }

        if (!$this->validateFileType($newFileName)) {
            throw new ApplicationException(Lang::get('cms::lang.media.type_blocked'));
        }

        $this->asExtension('FormController')->update_onSave(post('record_id'));

        return $this->listRefresh();
    }

    /**
     * Validate a proposed media item file name.
     * @param string
     * @return bool
     */
    protected function validateFileName($name)
    {
        if (!preg_match('/^[0-9a-z@\.\s_\-]+$/i', $name)) {
            return false;
        }

        if (strpos($name, '..') !== false) {
            return false;
        }

        return true;
    }

    /**
     * Check for blocked / unsafe file extensions
     * @param string
     * @return bool
     */
    public function validateFileType($name)
    {
        $extension = strtolower(File::extension($name));

        $allowedFileTypes = FileDefinitions::get('defaultExtensions');

        if (!in_array($extension, $allowedFileTypes)) {
            return false;
        }

        return true;
    }
}
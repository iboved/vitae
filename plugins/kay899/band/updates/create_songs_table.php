<?php namespace Kay899\Band\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSongsTable extends Migration
{
    public function up()
    {
        Schema::create('kay899_band_songs', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('sort_order')->unsigned()->nullable();
            $table->string('title');
            $table->string('artist');
            $table->string('internal_remark')->nullable();
            $table->string('duration_orig', 5)->nullable();
            $table->string('duration_rab', 5)->nullable();
            $table->string('first_key', 5)->nullable();
            $table->string('tempo', 5)->nullable();
            $table->text('text')->nullable();
            $table->tinyInteger('topic')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->boolean('is_public')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kay899_band_songs');
    }
}
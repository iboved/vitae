<?php namespace Stargate\Project\Components;

use Cms\Classes\ComponentBase;
use SystemException;

class Detail extends ComponentBase
{
    /**
     * @var item object
     */
    public $item;

    public function componentDetails()
    {
        return [
            'name'        => 'Details',
            'description' => 'Detail Darstellung aller Inhalte'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $url = $this->getRouter()->getUrl();

        $item = \Stargate\Project\Models\Slug::where('slug', $url)->first();

        if (!$item)
        {
            return \Response::make($this->controller->run('404'), 404);
        }

        $this->item = $this->page['item'] = $item->sluggable;
    }
}

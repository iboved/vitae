<?php namespace Vitae\Application\Controllers;

use Backend;
use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Support\Facades\Lang;
use October\Rain\Support\Facades\Flash;
use System\Classes\SettingsManager;
use Vitae\Application\Models\Preference as PreferenceModel;

/**
 * Preferences Back-end Controller
 */
class Preferences extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
    ];

    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();

        $this->addJs('/modules/backend/assets/js/preferences/preferences.js', 'core');

        $this->pageTitle = 'vitae.application::lang.preferences.model.title';

        BackendMenu::setContext('Vitae.Application', 'application', 'preferences');

        SettingsManager::setContext('October.Backend', 'preferences');
    }

    public function index()
    {
        $this->asExtension('FormController')->update();
    }

    public function formFindModelObject()
    {
        return PreferenceModel::instance();
    }

    public function index_onSave()
    {
        return $this->asExtension('FormController')->update_onSave();
    }

    public function index_onResetDefault()
    {
        $model = $this->formFindModelObject();
        $model->resetDefault();

        Flash::success(Lang::get('backend::lang.form.reset_success'));

        return Backend::redirect('vitae/application/preferences');
    }
}

<?php namespace Kay899\Band\Controllers;

use Backend;
use BackendMenu;
use Backend\Classes\Controller;
use Kay899\Band\Models\Song;

/**
 * Songs Back-end Controller
 */
class Songs extends Controller
{
    /**
     * @var \Backend\Classes\Controller Reference to the back end controller.
     */
    protected $controller;

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
        'Backend\Behaviors\ReorderController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kay899.Band', 'band', 'songs');

        $this->addJs('/plugins/kay899/band/assets/js/chordpro.js');
    }

    public function index()
    {
        $this->vars['suggestedSongsCount'] = Song::where('status', 1)->count();
        $this->vars['inProgressSongsCount'] = Song::where('status', 2)->count();
        $this->vars['setlistSongsCount'] = Song::where('status', 3)->count();

        $this->vars['duetSongsCount'] = Song::where('status', '!=', 1)->where('topic', 1)->count();
        $this->vars['lindaSongsCount'] = Song::where('status', '!=', 1)->where('topic', 2)->count();
        $this->vars['andreasSongsCount'] = Song::where('status', '!=', 1)->where('topic', 3)->count();

        $this->vars['songsCount'] = Song::count();

        $this->vars['isSortable'] = Song::count() > 1;
        $this->vars['status'] = array_reverse(Song::getStatusOptions(), true);

        $this->asExtension('ListController')->index();
    }

    public function printpreview()
    {
        $this->layout = '';
        
        $this->pageTitle = trans('kay899.band::lang.songs.printpreview.page_title');

        $this->vars['suggestedSongs'] = Song::where('status', 1)->orderBy('sort_order', 'asc')->get();
        $this->vars['inProgressSongs'] = Song::where('status', 2)->orderBy('sort_order', 'asc')->get();
        $this->vars['setlistSongs'] = Song::where('status', 3)->orderBy('sort_order', 'asc')->get();
    }

    public function reorderExtendQuery($query)
    {
        if (post('status')) {
            $query->where('status', post('status'));
        }
    }

    /**
     * Adding Form to Modal window for reorder
     *
     * @return mixed
     */
    public function onLoadReorder() 
    {
        $this->asExtension('ReorderController')->reorder();

        return $this->makePartial('form_reorder');
    }
}
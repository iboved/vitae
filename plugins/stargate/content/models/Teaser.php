<?php namespace Stargate\Content\Models;

use Model;
use Octobase\Categories\Models\Category;
use Stargate\Project\Models\Modelsetting;

/**
 * Teaser Model
 */
class Teaser extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    use \Stargate\Project\Traits\Base;

    // TODO: BaseTRait? evtl auch mit sluggable variable umbauen? dann sluggable Trait weg?

    protected $dates = ['deleted_at'];

    /**
     * Validation rules
     */
    public $rules = [
        'title' => 'required',
        'link' => 'required',
        'categories' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'stargate_content_teasers';

    public $morphToMany = [
        'categories' => ['Octobase\Categories\Models\Category', 'name' => 'categorizeable'],
    ];
    
    public $belongsTo = [
        'image' => ['Octobase\Mediamanager\Models\Image'],
    ];

    public function getTeaserCategoriesOptions($value, $formData)
    {
        $category = Category::find(ModelSetting::get('teasers_category'));
        
        return $category->getChildren()->pluck('title', 'id');
    }

    public function getCategoriesOptions()
    {
        $category = Category::find(ModelSetting::get('teasers_category'));

        $values = $category->getChildren()->pluck('title', 'id')->toArray();

        return $values;
    }

    public function getCategoriesLabelAttribute()
    {
        $values = $this->categories->pluck('title');

        $list = join(' | ', array_flatten($values));

        return $list;
    }

    public function scopeFilterCategories($query, array $categories)
    {
        return $query->whereHas('categories', function ($query) use ($categories) {
            $query->whereIn('id', $categories);
        });
    }
}

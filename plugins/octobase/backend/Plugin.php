<?php namespace Octobase\Backend;

use Backend;
use System\Controllers\Settings;
use System\Classes\PluginBase;
use Backend\Widgets\Lists;
use Octobase\Backend\Models\Setting;

/**
 * Backend Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = ['Inetis.ListSwitch'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'octobase.backend::lang.plugin.name',
            'description' => 'octobase.backend::lang.plugin.description',
            'author'      => 'Octobase',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        // Settings navigation show only 1 column a start (build 420 waste)
        if (Setting::get('backend_settings_one_column_layout'))
        {
            Settings::extend(function ($controller)
            {
                $controller->addJs('/plugins/octobase/backend/assets/js/settings.js');
            });
        }

        // Adding customViewPath for Lists (used by Inetis ListSwitch Plugin)
        Lists::extend(function($lists)
        {
            if (!$lists->getController() instanceof Categories)
            {
                return;
            }

            $lists->customViewPath = '$/octobase/backend/resources/list';
        });
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'octobase.backend.manage_settings' => [
                'tab' => 'octobase.backend::lang.plugin.backend_settings',
                'label' => 'octobase.backend::lang.settings.model.title'
            ],
        ];
    }

    /**
     * Registers setting items for this plugin.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'octobase.backend::lang.settings.model.title',
                'description' => 'octobase.backend::lang.settings.model.description',
                'category'    => 'octobase.backend::lang.plugin.name',
                'icon'        => 'icon-cog',
                'class'       => 'Octobase\Backend\Models\Setting',
                'order'       => 1,
                'permissions' => ['octobase.backend.manage_settings']
            ]
        ];
    }
}

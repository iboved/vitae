<?php

return [
    'plugin' => [
        'name' => 'MediaManager',
        'description' => 'Octobase MediaManager Erweiterungen',
    ],
    'images' => [
        'model' => [
            'name' => 'Bild',
            'title' => 'Bilder',
            'create' => 'Erstelle Bild',
            'update' => 'Bearbeite Bild',
            'reorder' => 'Bilder sortieren',
        ],
        'labels' => [
            'title' => 'Titel',
            'file_name' => 'Dateiname',
            'file_url' => 'Bildvorschau',
            'folder' => 'Ordner',
            'copyright' => 'Copyright',
        ],
        'validation' => [
            'title_cant_be_empty' => 'Der Titel darf nicht leer sein',
        ],
    ],
    'folders' => [
        'model' => [
            'name' => 'Ordner',
            'title' => 'Ordner',
            'create' => 'Erstelle Ordner',
            'update' => 'Bearbeite Ordner',
            'reorder' => 'Ordner sortieren',
        ],
        'labels' => [
            'title' => 'Titel',
            'folder_name' => 'Ordner',
            'images_count' => 'Anzahl Bilder',
            'is_gallery' => 'Ist Bildergalerie',
        ],
        'validation' => [
            'title_cant_be_empty' => 'Der Titel darf nicht leer sein',
        ],
    ],
    'videos' => [
        'model' => [
            'name' => 'Youtube-Video',
            'title' => 'Youtube-Videos',
            'create' => 'Erstelle Youtube-Video',
            'update' => 'Bearbeite Youtube-Video',
            'reorder' => 'Youtube-Videos sortieren',
        ],
        'labels' => [
            'title' => 'Title',
            'text' => 'Text',
            'videolink' => 'Video-Link',
            'sort_order' => 'Sortierung',
            'active' => 'Aktiv',
        ],
        'validation' => [
            'title_cant_be_empty' => 'Der Titel darf nicht leer sein',
            'videolink_cant_be_empty' => 'Der Video-Link darf nicht leer sein',
        ],
    ],
    'buttons' => [
        'without_title' => 'Bilder ohne Titel',
    ],
    'filters' => [
        'without_title' => 'Bilder ohne Titel',
        'folder' => 'Ordner',
    ],
    'settings' => [
        'tabs' => [
            'mediamanager' => 'MediaManager',
        ],
        'labels' => [
            'is_active' => 'MediaManager Erweiterung aktiv',
            'show_is_gallery' => 'Zeige Bildergalerie',
            'show_copyright' => 'Zeige Copyright',
            'show_youtube_module' => 'Zeige Youtube Modul an',
            'show_youtube_is_active' => 'Zeige Aktiv für Youtube Modul',
        ],
    ],
];
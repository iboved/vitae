<?php namespace Octobase\Backend\Behaviors;

use Backend;
use ApplicationException;
use Backend\Classes\ControllerBehavior;

class ModalController extends ControllerBehavior
{
    /**
     * @var \Backend\Classes\Controller|FormController Reference to the back end controller.
     */
    protected $controller;

    /**
     * Behavior constructor
     * @param Backend\Classes\Controller $controller
     */
    public function __construct($controller)
    {
        parent::__construct($controller);
    }

    // Adding Form to Modal window for create
    public function onCreateForm()
    {
        $this->controller->asExtension('FormController')->create();

        return $this->controller->makePartial('form_create');
    }

    // Saving data for create from Modal window
    public function onCreate()
    {
        return $this->controller->asExtension('FormController')->create_onSave();
    }

    // Adding Form to Modal window for udpate
    public function onUpdateForm()
    {
        $this->controller->asExtension('FormController')->update(post('record_id'));

        $this->controller->vars['recordId'] = post('record_id');

        return $this->controller->makePartial('form_update');
    }

    // Saving data for update from Modal window
    public function onUpdate()
    {
        $this->controller->asExtension('FormController')->update_onSave(post('record_id'));

        return $this->controller->listRefresh();
    }

    // Adding Form to Modal window for udpate
    public function onLoadReorder() 
    {
        $this->controller->reorder();

        return $this->controller->makePartial('form_reorder');
    }

}
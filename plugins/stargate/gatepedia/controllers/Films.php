<?php namespace Stargate\Gatepedia\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Films Back-end Controller
 */
class Films extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend\Behaviors\ReorderController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'stargate.gatepedia.manage_gatepedia'
    ];

    public function __construct()
    {
        parent::__construct();

        $this->bodyClass = 'compact-container';

        BackendMenu::setContext('Stargate.Gatepedia', 'gatepedia', 'films');
    }
}

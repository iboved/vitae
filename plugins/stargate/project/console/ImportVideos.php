<?php namespace Stargate\Project\Console;

use DB;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Octobase\Mediamanager\Models\Video;

class ImportVideos extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'project:importvideos';

    /**
     * @var string The console command description.
     */
    protected $description = 'Import Script for Videos from SG-P to October version';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {

            #$videos = DB::connection('mysql_old')->table('videos')->where('id', 5)->get();
            $videos = DB::connection('mysql_old')->table('videos')->get();

            DB::table('octobase_mediamanager_videos')->truncate();

            foreach ($videos as $item) {

                $new = new Video();

                $new->id = $item->id;
                $new->title = $item->title;
                $new->text = $item->teasertext;
                $new->videolink = $item->videolink;
                $new->sort_order = $item->id;
                $new->is_active = $item->active;
                $new->created_by = $item->created_by;
                $new->updated_by = $item->updated_by;
                $new->deleted_by = $item->deleted_by;
                $new->created_at = $item->created_at;
                $new->updated_at = $item->updated_at;
                $new->deleted_at = $item->deleted_at;
                $new->save();
            }
        });

        $this->output->writeln('Videos imported');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}

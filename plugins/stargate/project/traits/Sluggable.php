<?php namespace Stargate\Project\Traits;

use Backend\Facades\BackendAuth;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use ReflectionClass;
use Stargate\Project\Traits\Base;
use Stargate\Project\Models\Slug;
use October\Rain\Exception\ValidationException;

trait Sluggable
{
    use Base {
        Base::beforeCreate as traitBeforeCreate;
        Base::beforeUpdate as traitBeforeUpdate;
        Base::beforeDelete as traitBeforeDelete;
    }

    public $slugInstance;

    public function beforeCreate()
    {
        if (php_sapi_name() != 'cli') {
            $slugname = request()->get($this->sluggableModel)['sluggable'];
            $sluggable = Slug::create(['slug' => $slugname]);

            unset($this->sluggable);

            $data = [
                'title' => $this->title,
                'slug' => $slugname,
                'sluggable_id' => 0,
                'sluggable_type' => (new ReflectionClass($this))->getShortName(),
                'user_id' => BackendAuth::getUser()->id
            ];

            $client = new Client();
            try {
                $response = $client->post('http://api.stargate-project.de/ssa/ffd7f5af-4007-425c-a5fe-69d95326f336', [
                    RequestOptions::JSON => $data
                ]);
            } catch (BadResponseException $e) {
                throw new ValidationException(['message' => 'Der Slug ist bereits vorhanden']);
            }

            $this->slugInstance = $sluggable;
        }

        $this->traitBeforeCreate();
    }

    public function afterCreate()
    {
        if (php_sapi_name() != 'cli') {
            $this->slug()->save($this->slugInstance);

            $data = [
                'title' => $this->title,
                'slug' => $this->slugInstance->slug,
                'sluggable_id' => $this->id,
                'sluggable_type' => (new ReflectionClass($this))->getShortName(),
                'user_id' => BackendAuth::getUser()->id
            ];

            $client = new Client();
            try {
                $response = $client->put('http://api.stargate-project.de/ssa/ffd7f5af-4007-425c-a5fe-69d95326f336', [
                    RequestOptions::JSON => $data
                ]);
            } catch (BadResponseException $e) {
                throw new ValidationException(['message' => 'Slug konnte nicht mit dem alten System synchronisiert werden']);
            }
        }
    }

    public function beforeUpdate()
    {
        $this->traitBeforeUpdate();
    }

    public function beforeDelete()
    {
        $this->traitBeforeDelete();
    }

    public function beforeValidate()
    {
        if ($this->exists)
        {
            unset($this->rules['sluggable']);
        }
    }

    public function getSluggableAttribute()
    {
        return ($this->slug->count()) ? $this->slug[0]->slug : '';
    }

}

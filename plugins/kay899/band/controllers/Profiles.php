<?php namespace Kay899\Band\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Backend\Controllers\Users as BackendUser;

/**
 * Profiles Back-end Controller
 */
class Profiles extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kay899.Band', 'band', 'profiles');
    }
}

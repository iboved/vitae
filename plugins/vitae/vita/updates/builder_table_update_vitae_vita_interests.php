<?php namespace Vitae\Vita\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVitaeVitaInterests extends Migration
{
    public function up()
    {
        Schema::table('vitae_vita_interests', function($table)
        {
            $table->integer('sort_order')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('vitae_vita_interests', function($table)
        {
            $table->integer('sort_order')->default(null)->change();
        });
    }
}

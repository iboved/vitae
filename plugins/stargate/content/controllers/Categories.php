<?php namespace Stargate\Content\Controllers;

use BackendMenu;
use Octobase\Categories\Controllers\Categories as BaseCategories;

/**
 * Categories Back-end Controller
 */
class Categories extends BaseCategories
{
    public function __construct()
    {
        parent::__construct();

        $this->addViewPath(plugins_path('octobase/categories/controllers/categories'));
        $this->viewPath = array_reverse($this->viewPath);

        BackendMenu::setContext('Stargate.Content', 'content', 'categories');
    }
}

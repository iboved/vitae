<?php namespace Stargate\Project\Console;

use DB;
use Hash;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Backend\Models\User;

class ImportUsers extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'project:importusers';

    /**
     * @var string The console command description.
     */
    protected $description = 'Import Script for User from SG-P to October version';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {

            #$users = DB::connection('mysql_old')->table('users')->where('id', 1)->get();
            $users = DB::connection('mysql_old')->table('users')->get();

            DB::table('backend_users')->truncate();

            foreach ($users as $item) {
                $new = new User();
                $new->id = $item->id;
                $new->login = $item->username;
                $new->email = $item->email;
                $new->password = 'sgp2017';
                $new->password_confirmation = 'sgp2017';
                $new->is_activated = $item->team_type != 2 ? 1 : 0;
                $new->role_id = $item->role_id;
                $new->activated_at = $item->created_at;
                $new->created_at = $item->created_at;
                $new->updated_at = $item->updated_at;
                $new->is_superuser = $item->role_id == 1 ? 1 : 0;
                $new->save();
            }
        });

        $this->output->writeln('Users imported');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}

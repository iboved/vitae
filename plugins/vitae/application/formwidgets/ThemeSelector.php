<?php namespace Vitae\Application\FormWidgets;

use Backend\Classes\FormField;
use Backend\Classes\FormWidgetBase;
use Cms\Classes\Theme;
use October\Rain\Support\Facades\Flash;

/**
 * ThemeSelector Form Widget
 */
class ThemeSelector extends FormWidgetBase
{
    //
    // Configurable properties
    //

    /**
     * @var array themes that should not be displayed.
     */
    public $exceptThemes = [];

    /**
     * @var string name of the theme color column.
     */
    public $colorColumn = 'theme_color';

    //
    // Object properties
    //

    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'themeselector';

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig([
            'exceptThemes',
            'colorColumn',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();

        return $this->makePartial('themeselector');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
        $this->vars['themes'] = $this->getFilteredThemes();
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/themeselector.css', 'Vitae.Application');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return FormField::NO_SAVE_DATA;
    }

    public function onSetActiveTheme()
    {
        if (Theme::exists(post('theme'))) {
            $fieldName = $this->formField->fieldName;
            $this->model->$fieldName = post('theme');

            if (post('color')) {
                $this->model->{$this->colorColumn} = post('color');
            } else {
                $this->model->{$this->colorColumn} = null;
            }

            $this->model->save();

            // Necessary for compatibility with Settings Models
            $this->model->$fieldName = post('theme');

            if (post('color')) {
                $this->model->{$this->colorColumn} = post('color');
            }

            return [
                '#theme-list' => $this->makePartial('theme_list', ['themes' => $this->getFilteredThemes()])
            ];
        } else {
            Flash::error(trans('vitae.application::lang.preferences.validation.theme_not_found'));
        }
    }

    /**
     * @return array
     */
    public function getFilteredThemes()
    {
        return array_where(Theme::all(), function ($value, $key) {
            return !in_array($value->getDirName(), $this->exceptThemes);
        });
    }

    /**
     * Returns true if this theme is the chosen active theme.
     *
     * @param $themeDirName
     * @return bool
     */
    public function isActiveTheme($themeDirName)
    {
        $fieldName = $this->formField->fieldName;
        $activeTheme = $this->model->$fieldName;

        return $activeTheme == $themeDirName;
    }

    /**
     * Returns true if this theme color is the chosen active theme color.
     *
     * @param $themeColor
     * @return bool
     */
    public function isActiveThemeColor($themeColor)
    {
        $activeThemeColor = $this->model->{$this->colorColumn};

        return $activeThemeColor == $themeColor;
    }
}

<?php namespace Vitae\Dashboard;

use Backend;
use System\Classes\PluginBase;

/**
 * Dashboard Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Dashboard',
            'description' => 'No description provided yet...',
            'author'      => 'Vitae',
            'icon'        => 'icon-leaf'
        ];
    }

    public function registerReportWidgets()
    {
        return [
            'Vitae\Dashboard\ReportWidgets\CustomerSelect' => [
                'label'   => 'Customer Select',
                'context' => 'vitae'
            ],
        ];
    }

}

<?php namespace Kay899\Band\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGigsTable extends Migration
{
    public function up()
    {
        Schema::create('kay899_band_gigs', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->dateTime('datetime')->nullable();
            $table->text('teasertext')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('sets')->default(0);
            $table->string('location')->nullable();
            $table->text('location_details')->nullable();
            $table->string('location_link')->nullable();
            $table->string('entrance_fee')->nullable();
            $table->string('facebook')->nullable();
            $table->boolean('is_public')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kay899_band_gigs');
    }
}
